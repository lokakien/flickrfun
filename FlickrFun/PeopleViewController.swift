//
//  PeopleViewController.swift
//  FlickrFun
//
//  Created by Loka on 07/05/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit
import MBProgressHUD

class PeopleViewController: UIViewController {
    var people: People!
    var currentPage: Int = 1
    var totalPages: Int = 2
    var refreshControl: UIRefreshControl!
    var selectedSegment: Int = 0
    
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = UIColor.whiteColor()
        if people.fullname != nil && people.fullname != ""{
            self.navigationItem.title = people.fullname
        }else{
            self.navigationItem.title = people.username
        }
        refreshData()
        refreshControl = pullDownRefresh(self, selector: #selector(refreshData))
        collectionView.addSubview(refreshControl)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismissVC(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func refreshData(){
        Flickr.sharedInstance().getPeopleInfo(people, completionHandler: {
            success in
            dispatch_async(dispatch_get_main_queue(), {
                self.collectionView.reloadData()
            })
        })
        Flickr.sharedInstance().getFollower(people, completionHandler: { (followers) in
            //
            self.people.followers = followers
            dispatch_async(dispatch_get_main_queue(), {
                self.collectionView.reloadData()
            })
            
        })
        Flickr.sharedInstance().getUserPublicFollowing(people, completionHandler: { (following) in
            self.people.following = following
            dispatch_async(dispatch_get_main_queue(), {
                self.collectionView.reloadData()
            })
            
        })
        Flickr.sharedInstance().getPhotosInfo(people, page: currentPage, totalPages: totalPages) { (photos, currentPage, totalPages) in
            self.people.photos = photos
            self.currentPage = currentPage
            self.totalPages = totalPages
            self.refreshControl.endRefreshing()
            dispatch_async(dispatch_get_main_queue(), {
                self.collectionView.reloadData()
            })
            
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "seguePeopleVCToPhotoVC"{
            let photoVC = segue.destinationViewController as! PhotoViewController
            photoVC.photos = people.photos
            let selected = collectionView.indexPathsForSelectedItems()?.first!.item
            photoVC.selected = selected
        }
        if segue.identifier == "seguePeopleVCToAlbumDetailVC"{
            let albumDetailVC = segue.destinationViewController as! AlbumDetailViewController
             let selected = collectionView.indexPathsForSelectedItems()!.first!.item
            albumDetailVC.album = people.albums[selected]
            albumDetailVC.people = people
        }
        if segue.identifier == "seguePeopleVCToGroupDetailVC"{
            let navigationController = segue.destinationViewController as! UINavigationController
            let groupDetailVC = navigationController.viewControllers.first as! GroupDetailViewController
            let selected = collectionView.indexPathsForSelectedItems()!.first!.item
            groupDetailVC.group = people.groups[selected]
            groupDetailVC.people = people
        }
    }
    

    @IBAction func segmentChange(sender: UISegmentedControl) {
        self.selectedSegment = sender.selectedSegmentIndex
        if sender.selectedSegmentIndex == 1{
            if people.albums.isEmpty{
                Flickr.sharedInstance().getAlbums(people, currentPage: people.albumsCurrentPage, totalPages: people.albumsTotalPages, completionHandler: { (albums, currentPage, totalPages) in
                    self.people.albums = albums
                    self.people.albumsCurrentPage = currentPage
                    self.people.albumsTotalPages = totalPages
                    self.collectionView.reloadData()
                })
            }
        }
        if sender.selectedSegmentIndex == 2{
            if people.groups.isEmpty{
                Flickr.sharedInstance().getGroupsForPeople(people, completionHandler: { groups in
                    self.people.groups = groups
                    self.collectionView.reloadData()
                })
            }
        }
        collectionView.reloadData()
    }
    

}

extension PeopleViewController: UICollectionViewDataSource{
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 3
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 2 {
            switch selectedSegment {
            case 0:
                return people.photos.count
            case 1:
                return people.albums.count
            case 2:
                return people.groups.count
            default:
                print ("undefined")
            }
            
        }
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        var cell: UICollectionViewCell!
        switch indexPath.section {
        case 0:
            let reuseCell = collectionView.dequeueReusableCellWithReuseIdentifier("peopleProfileCell", forIndexPath: indexPath) as! ProfileCell
            reuseCell.people = people
            reuseCell.delegate = self
            cell = reuseCell
            break
        case 1:
            let reusecell = collectionView.dequeueReusableCellWithReuseIdentifier("peopleSegmentCell", forIndexPath: indexPath)
            cell = reusecell
        case 2:
            if selectedSegment == 0{
                let reuseCell = collectionView.dequeueReusableCellWithReuseIdentifier("photoCell", forIndexPath: indexPath) as! PhotoCell
                reuseCell.photo = people.photos[indexPath.item]
                cell = reuseCell
            }
            if selectedSegment == 1{
                let reuseCell = collectionView.dequeueReusableCellWithReuseIdentifier("albumCollectionViewCell", forIndexPath: indexPath) as! AlbumCollectionViewCell
                reuseCell.album = people.albums[indexPath.item]
                cell = reuseCell
            }
            if selectedSegment == 2{
                let reuseCell = collectionView.dequeueReusableCellWithReuseIdentifier("groupCollectionViewCell", forIndexPath: indexPath) as! GroupCollectionViewCell
                reuseCell.group = people.groups[indexPath.item]
                cell = reuseCell
            }
            break
        default:
            print ("undefined")
        }
        return cell
    }
}

extension PeopleViewController: UICollectionViewDelegateFlowLayout{
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        var size: CGSize = CGSizeZero
        switch indexPath.section{
        case 0:
            size = CGSizeMake(collectionView.frame.width, collectionView.frame.height/4)
            break
        case 1:
            size = CGSizeMake(collectionView.frame.width, 40)
            break
        case 2:
            if selectedSegment == 0{
                size = CGSizeMake(collectionView.frame.width/3, collectionView.frame.width/3)
            }else{
                size = CGSizeMake(collectionView.frame.width, 90)
            }
            
        default:
            size = CGSizeZero
        }
        return size
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
}

extension PeopleViewController: UICollectionViewDelegate{
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 2{
            if selectedSegment == 0{
                self.performSegueWithIdentifier("seguePeopleVCToPhotoVC", sender: self)
            }
            if selectedSegment == 1{
                self.performSegueWithIdentifier("seguePeopleVCToAlbumDetailVC", sender: self)
            }
            if selectedSegment == 2{
                self.performSegueWithIdentifier("seguePeopleVCToGroupDetailVC", sender: self)
            }
        }
    }
}


