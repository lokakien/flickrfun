//
//  ProfileOptionCell.swift
//  FlickrFun
//
//  Created by Loka on 06/05/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

protocol ProfileOptionCellDelegate{
    func profileOptionCell(cell: ProfileOptionCell, didPressSelect button: UIButton)
    func profileOptionCell(cell: ProfileOptionCell, didPressDisplay button: UIButton)
    func profileOptionCell(cell: ProfileOptionCell, didPressFilter button: UIButton)
}

class ProfileOptionCell: UICollectionViewCell {
    
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var displayButton: UIButton!
    @IBOutlet weak var filterButton: UIButton!
    
    var delegate: ProfileOptionCellDelegate?
    
    @IBAction func selectButtonPressed(sender: AnyObject) {
        if let delegate = self.delegate{
            delegate.profileOptionCell(self, didPressSelect: sender as! UIButton)
        }
        
    }
    
    @IBAction func displayButtonPressed(sender: AnyObject) {
        if let delegate = self.delegate{
            delegate.profileOptionCell(self, didPressDisplay: sender as! UIButton)
        }
    }
    
    @IBAction func filterButtonPressed(sender: AnyObject) {
        if let delegate = self.delegate{
            delegate.profileOptionCell(self, didPressFilter: sender as! UIButton)
        }
    }
    
    
    
}
