//
//  AlbumViewController.swift
//  FlickrFun
//
//  Created by Loka on 06/05/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

class AlbumViewController: UIViewController {
    var people: People!
    var refreshControl: UIRefreshControl?
    
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        people = User.sharedInstance()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.alwaysBounceVertical = true
        refreshData()
        refreshControl = pullDownRefresh(self, selector: #selector(refreshData))
        tableView.addSubview(refreshControl!)
    
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBar.hidden = true
        if let indexPath = tableView.indexPathForSelectedRow{
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
        }
       tableView.reloadData()
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.navigationBar.hidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func refreshData(){
        Flickr.sharedInstance().getAlbums(people, currentPage: 1, totalPages: people.albumsTotalPages) { (albums, currentPage, totalPages) in
            self.refreshControl?.endRefreshing()
            guard !albums.isEmpty else{
                return
            }
            var offset = 0
            if self.people.albums.isEmpty{
                offset = 0
            }else{
                if self.people.albums[0].id == "faves"{
                    offset = 1
                }
            }
            for i in 0...albums.count - 1{
                if i+offset < self.people.albums.count {
                    self.people.albums[i+offset] = albums[i]
                }else {
                    self.people.albums.append(albums[i])
                }
            }
            self.tableView.reloadData()
            self.people.albumsTotalPages = totalPages
            self.people.albumsCurrentPage = currentPage
        }
//        Flickr.sharedInstance().getAlbums(1, totalPages: people.albumsTotalPages) { (albums, currentPage, totalPages) in
//            
//        }
        Flickr.sharedInstance().getFavesAlbum(1) { (album) in
            if let album = album {
                if self.people.albums.isEmpty{
                    self.people.albums.append(album)
                }else{
                    if self.people.albums[0].id == "faves"{
                        self.people.albums[0] = album
                    }else{
                        self.people.albums.insert(album, atIndex: 0)
                    }
                }
                
            }else{
                if !self.people.albums.isEmpty{
                    if self.people.albums[0].id == "faves"{
                        self.people.albums.removeAtIndex(0)
                    }
                }
            }
            self.tableView.reloadData()
        }
    }
    
}

extension AlbumViewController: UITableViewDataSource{
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = people.albums.count
        if count == 0{
            tableView.backgroundView = getBackgroundLabel(tableView)
        }else{
            tableView.backgroundView = nil
        }
        return count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("albumCell") as! AlbumCell
        cell.album = people.albums[indexPath.row]
        return cell
    }
    
}

extension AlbumViewController: UITableViewDelegate{
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let height = self.view.frame.width/3
        return height
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("segueAlbumToAlbumDetail", sender: self)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueAlbumToAlbumDetail"{
            let albumDetailVC = segue.destinationViewController as! AlbumDetailViewController
            albumDetailVC.album = people.albums[tableView.indexPathForSelectedRow!.row]
            albumDetailVC.people = people
        }
    }
}
