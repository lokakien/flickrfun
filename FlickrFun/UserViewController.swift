//
//  MainViewController.swift
//  FlickrOffline
//
//  Created by Loka on 21/04/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit
import OAuthSwift
import MBProgressHUD
import SDWebImage

class UserViewController: UIViewController {
    
    
    @IBOutlet weak var menuBarItem: UIBarButtonItem!
    @IBOutlet weak var collectionView: UICollectionView!
    var leftSwipe = UISwipeGestureRecognizer()
    var people: People!
    var currentPage: Int = 1
    var totalPages: Int = 2
    var editMode: Bool = false
    var subTabbar: UITabBar!
    var photoFilterType: Photo.permissionType = .All
    var refreshControl: UIRefreshControl?
    
    enum subTabbarItem{
        case Lock
        case Album
        case Share
        case Download
        case Delete
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.alwaysBounceVertical = true
        people = User.sharedInstance()
        self.navigationItem.title = people.fullname
        refreshData()
        refreshControl = pullDownRefresh(self, selector: #selector(refreshData))
        collectionView.addSubview(refreshControl!)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        leftSwipe.direction = UISwipeGestureRecognizerDirection.Left
        leftSwipe.addTarget(self, action: #selector(self.swiped))
        self.view.addGestureRecognizer(leftSwipe)
        collectionView.reloadData()
        if editMode == true{
            toggleEditMode()
        }
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        self.view.removeGestureRecognizer(leftSwipe)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func swiped() {
        self.tabBarController?.selectedIndex = self.tabBarController!.selectedIndex + 1
    }
    
    func refreshData(){
        Flickr.sharedInstance().getPhotosInfo(User.sharedInstance(), page: 1, totalPages: totalPages) { (photos, currentPage, totalPages) in
            self.people.photos = photos
            self.currentPage = currentPage
            self.totalPages = totalPages
            let mainQueue = dispatch_get_main_queue()
            dispatch_async(mainQueue, {
                self.collectionView.reloadData()
            })
        }
        Flickr.sharedInstance().getFollower(people, completionHandler: { (followers) in
            //
            self.people.followers = followers
            self.collectionView.reloadData()
        })
        Flickr.sharedInstance().getUserFollowing(people, completionHandler: { (following) in
            self.people.following = following
            self.collectionView.reloadData()
        })
        Flickr.sharedInstance().getPeopleInfo(people, completionHandler: {
            success in 
            self.collectionView.reloadData()
            if let refreshControl = self.refreshControl{
                refreshControl.endRefreshing()
            }
        })
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueUserVCToPhotoVC"{
            let photoVC = segue.destinationViewController as! PhotoViewController
            photoVC.photos = getFilteredPhoto()
            let indexPath = collectionView.indexPathsForSelectedItems()!.first
            photoVC.selected = indexPath!.item
        }
        
        if segue.identifier == "segueUserVCToAddToAlbumVC"{
            let addToAlbumVC = segue.destinationViewController as! AddToAlbumViewController
            addToAlbumVC.people = people
            let filteredPhotos: [Photo] = getFilteredPhoto()
            let selectedIndexPaths: [NSIndexPath] = collectionView.indexPathsForSelectedItems()!
            var selectedPhotos: [Photo] = []
            for indexPath in selectedIndexPaths{
                selectedPhotos.append(filteredPhotos[indexPath.item])
            }
            addToAlbumVC.selectedPhotos = selectedPhotos
        }
    }
    
    func getFilteredPhoto()->[Photo]{
        var photos: [Photo] = []
        if self.photoFilterType == Photo.permissionType.All{
            photos = people.photos
        }else{
            photos = people.photos.filter{$0.permission == self.photoFilterType}
        }
        return photos
    }
    
    @IBAction func showMenu(sender: AnyObject) {
//        let alertController = UIAlertController(title: "Menu", message: nil, preferredStyle: .ActionSheet)
        
    }
    
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        collectionView.collectionViewLayout.invalidateLayout()
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
    }
    
    
}

extension UserViewController: UICollectionViewDataSource{
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 3
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 2{
            let photos: [Photo] = getFilteredPhoto()
            let count = photos.count
            if count == 0 {
                collectionView.backgroundView = getBackgroundLabel(collectionView)
            }else {
                collectionView.backgroundView = nil
                collectionView.backgroundColor = UIColor.whiteColor()
            }
            return count
        }
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        var cell: UICollectionViewCell!
        if indexPath.section == 0{
            let reuseCell = collectionView.dequeueReusableCellWithReuseIdentifier("profileCell", forIndexPath: indexPath) as! ProfileCell
            reuseCell.people = people
            reuseCell.delegate = self
            cell = reuseCell
        }
        if indexPath.section == 1{
            let reuseCell = collectionView.dequeueReusableCellWithReuseIdentifier("profileOptionCell", forIndexPath: indexPath) as! ProfileOptionCell
            reuseCell.delegate = self
            cell = reuseCell
        }
        if indexPath.section == 2{
            let reuseCell = collectionView.dequeueReusableCellWithReuseIdentifier("photoCell", forIndexPath: indexPath) as! PhotoCell
            let photos: [Photo] = getFilteredPhoto()
            reuseCell.photo = photos[indexPath.item]
            let backgroundView = UIView(frame: reuseCell.frame)
            backgroundView.backgroundColor = UIColor.blueColor()
            reuseCell.selectedBackgroundView = backgroundView
            cell = reuseCell
        }
        return cell
    }
    
}

extension UserViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        var size: CGSize = CGSizeZero
        switch indexPath.section{
        case 0:
            size = CGSizeMake(collectionView.frame.width, collectionView.frame.height/4)
            break
        case 1:
            size = CGSizeMake(collectionView.frame.width, 40)
            break
        case 2:
            size = CGSizeMake(collectionView.frame.width/3, collectionView.frame.width/3)
        default:
            size = CGSizeZero
        }
        return size
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
}

extension UserViewController: UICollectionViewDelegate{
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 2{
            if editMode == false{
                self.performSegueWithIdentifier("segueUserVCToPhotoVC", sender: self)
            }else{
                let cell = collectionView.cellForItemAtIndexPath(indexPath) as! PhotoCell
                cell.photoImageResize()
            }
        }
    }
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        if editMode == true{
            let cell = collectionView.cellForItemAtIndexPath(indexPath) as! PhotoCell
            cell.photoImageResize()
        }
    }
    
    func scrollViewWillEndDragging(scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let scrollOffsetY = scrollView.contentOffset.y
        let triggerOffset: CGFloat = 20.0
        guard scrollOffsetY - triggerOffset > scrollView.contentSize.height - scrollView.frame.size.height else {
            return
        }
        if currentPage < totalPages {
            print("current page : \(currentPage)")
            MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            Flickr.sharedInstance().getPhotosInfo(User.sharedInstance(), page: currentPage + 1, totalPages: totalPages) { (photos, currentPage, totalPages) in
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                for photo in photos{
                    self.people.photos.append(photo)
                }
                self.currentPage = currentPage
                self.totalPages = totalPages
                let mainQueue = dispatch_get_main_queue()
                dispatch_async(mainQueue, {
                    self.collectionView.reloadData()
                })
            }
            
        }
    }
}

extension UserViewController: ProfileOptionCellDelegate{
    func profileOptionCell(cell: ProfileOptionCell, didPressSelect button: UIButton) {
        toggleEditMode()
    }
    
    func profileOptionCell(cell: ProfileOptionCell, didPressFilter button: UIButton) {
        //
        print ("Filter")
        let alertController = UIAlertController(title: "Filter Type", message: nil, preferredStyle: .ActionSheet)
        let alertArray = ["All View","Private View", "Family View", "Friend View", "Public View"]
        for i in 0...alertArray.count - 1{
            let action = UIAlertAction(title: alertArray[i], style: .Default, handler: setFilter)
            alertController.addAction(action)
        }
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    func profileOptionCell(cell: ProfileOptionCell, didPressDisplay button: UIButton) {
        //
        print ("Display")
    }
    
}

extension UserViewController: UITabBarDelegate{
    func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        let indexPath = collectionView.indexPathsForSelectedItems()
        guard indexPath?.count > 0 else {
            return
        }
        switch item.tag{
        case subTabbarItem.Lock.hashValue:
            print ("Lock")
            let alertController = UIAlertController(title: "Set Permision", message: nil, preferredStyle: .ActionSheet)
            let alertArray = ["Private View", "Family View", "Friend View", "Public View"]
            for i in 0...alertArray.count - 1{
                let action = UIAlertAction(title: alertArray[i], style: .Default, handler: setPermission)
                alertController.addAction(action)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: cancelEditMode)
            alertController.addAction(cancelAction)
            self.presentViewController(alertController, animated: true, completion: nil)
            break
        case subTabbarItem.Album.hashValue:
            print ("Album")
            let alertController = UIAlertController(title: "Add To Album", message: nil, preferredStyle: .ActionSheet)
            let alertArray = ["Add"]
            for i in 0...alertArray.count - 1{
                let action = UIAlertAction(title: alertArray[i], style: .Default, handler: addToAlbum)
                alertController.addAction(action)
            }
            self.presentViewController(alertController, animated: true, completion: nil)
            break
        //remember to change this to share image instead
        case subTabbarItem.Share.hashValue:
            print ("Share")
            var selectedImages: [String] = []
            let filteredPhotos: [Photo] = getFilteredPhoto()
            let indexPaths = collectionView.indexPathsForSelectedItems()
            for i in indexPaths!{
                if i.section == 2{
                    selectedImages.append(filteredPhotos[i.item].getLowResUrl().absoluteString)
                }
            }
            let activityController = UIActivityViewController.init(activityItems: selectedImages, applicationActivities:nil)
            self.presentViewController(activityController, animated: true, completion: self.toggleEditMode)
            break
        case subTabbarItem.Download.hashValue:
            print ("Download")
            break
        case subTabbarItem.Delete.hashValue:
            print ("Delete")
            let alertController = UIAlertController(title: "Delete Photo", message: nil, preferredStyle: .ActionSheet)
            let confirmAction = UIAlertAction(title: "Confirm", style: .Default, handler: deletePhoto)
            let cancelAcction = UIAlertAction(title: "Cancel", style: .Cancel, handler: cancelEditMode)
            alertController.addAction(confirmAction)
            alertController.addAction(cancelAcction)
            self.presentViewController(alertController, animated: true, completion: nil)
            break
        default:
            print ("undefined")
        }
    }
    
    func setPermission(alertAction: UIAlertAction){
        var permission: Photo.permissionType!
        switch alertAction.title! {
        case "Private View":
            permission = Photo.permissionType.isPrivate
            break
        case "Family View":
            permission = Photo.permissionType.isFamily
            break
        case "Friend View":
            permission = Photo.permissionType.isFriend
            break
        case "Public View":
            permission = Photo.permissionType.isPublic
            break
        default:
            print ("undefined")
        }
        let indexPathSelected = self.collectionView.indexPathsForSelectedItems()
        var photoArray: [Photo] = []
        for index in indexPathSelected! {
            if index.section == 2{
                photoArray.append(self.people.photos[index.item])
            }
        }
        self.toggleEditMode()
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        Flickr.sharedInstance().setPhotoPermission(photoArray, permission: permission, completionHandler: {
            success in
            MBProgressHUD.hideHUDForView(self.view, animated: true)
            dispatch_async(dispatch_get_main_queue(), {
                
                self.collectionView.reloadData()
            })
        })
    }
    
    func toggleEditMode(){
        editMode = !editMode
        if editMode{
            let imageName = ["Lock", "Album", "Share","Download", "Delete"]
            var items: [UITabBarItem] = []
            for i in 0...imageName.count - 1{
                let item = UITabBarItem(title: "", image: UIImage(named: imageName[i]), tag: i)
                items.append(item)
            }
            let frame = CGRectMake(self.collectionView.frame.origin.x, self.tabBarController!.tabBar.frame.origin.y - 40, self.collectionView.frame.width, 40)
            subTabbar = UITabBar(frame: frame)
            subTabbar.tintColor = UIColor.grayColor()
            subTabbar.delegate = self
            subTabbar.items = items
            self.view.addSubview(subTabbar)
            self.collectionView.allowsMultipleSelection  = true
        }else{
            if subTabbar != nil {
                subTabbar.removeFromSuperview()
            }
            let indexPaths = collectionView.indexPathsForSelectedItems()
            guard indexPaths!.count > 0 else{
                return
            }
            for i in 0...indexPaths!.count - 1{
                if indexPaths![i].section == 2{
                    let cell = collectionView(collectionView, cellForItemAtIndexPath: indexPaths![i]) as! PhotoCell
                    cell.selected = false
                    cell.photoImageResize()
                }
            }
            collectionView.reloadData()
            self.collectionView.allowsMultipleSelection = false
        }
    }
    
    
    func setFilter(alertAction: UIAlertAction){
        switch alertAction.title! {
        case "All View":
            photoFilterType = .All
            break
        case "Private View":
            photoFilterType = .isPrivate
            break
        case "Family View":
            photoFilterType = .isFamily
            break
        case "Friend View":
            photoFilterType = .isFamily
            break
        case "Public View":
            photoFilterType = .isPublic
            break
        default:
            print ("undefined")
        }
        self.collectionView.reloadData()
    }
    
    func addToAlbum(alertAction: UIAlertAction){
        self.performSegueWithIdentifier("segueUserVCToAddToAlbumVC", sender: self)
    }
    
    func deletePhoto(alertAction: UIAlertAction){
        let indexPathSelected = self.collectionView.indexPathsForSelectedItems()
        var selectedPhotos: [Photo] = []
        for index in indexPathSelected! {
            if index.section == 2{
                selectedPhotos.append(self.people.photos[index.item])
            }
        }
        self.toggleEditMode()
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        Flickr.sharedInstance().deletePhoto(selectedPhotos, people: people) { (success) in
            //success
            MBProgressHUD.hideHUDForView(self.view, animated: true)
            dispatch_async(dispatch_get_main_queue(), { 
               self.collectionView.reloadData()
            })
        }
    }
    
    func cancelEditMode(alertAction: UIAlertAction){
        toggleEditMode()
    }
    
}





