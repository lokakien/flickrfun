//
//  GroupCell.swift
//  FlickrFun
//
//  Created by Loka on 03/05/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

class GroupCell: UITableViewCell {
    
    @IBOutlet weak var photoLabel: UILabel!
    @IBOutlet weak var memberLabel: UILabel!
    @IBOutlet weak var groupImageView: UIImageView!

    @IBOutlet weak var titleLabel: UILabel!
    
    var group: Group!{
        didSet{
            groupImageView.sd_setImageWithURL(group.coverPhotoURL, placeholderImage: UIImage.init(named: "Placeholder"))
            titleLabel.text = group.title
            memberLabel.text = "\(group.numberOfGroupMembers) members"
            photoLabel.text = "\(group.numberOfPhotos) photos"
        }
    }
}
