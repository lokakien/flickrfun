//
//  Favourite.swift
//  FlickrFun
//
//  Created by Loka on 06/05/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

class Favourite{
    var peoples: [People]!
    var currentPage: Int!
    var totalPages: Int!
    
    init(){
        self.peoples = [People]()
        totalPages = 2
        currentPage = 1
    }
    
    
}
