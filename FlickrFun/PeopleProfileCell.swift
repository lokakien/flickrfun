//
//  PeopleProfileCell.swift
//  FlickrFun
//
//  Created by Loka on 07/05/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

class PeopleProfileCell: UICollectionViewCell {
    @IBOutlet weak var peopleImageView: UIImageView!
    
    @IBOutlet weak var photoLabel: UILabel!
    @IBOutlet weak var aboutLabel: UILabel!
    @IBOutlet weak var followerLabel: UILabel!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var followingLabel: UILabel!
    
    var people: People!{
        didSet{
            peopleImageView.layer.cornerRadius = peopleImageView.frame.width/2
            peopleImageView.sd_setImageWithURL(people.iconURL, placeholderImage: UIImage.init(named: "Placeholder"))
            updateDisplay()
        }
    }
    
    func updateDisplay(){
        guard people.following != nil && people.followers != nil && people.aboutPeople != nil  else{
            return
        }
        followerLabel.text = "\(people.followers!.total)\n followers"
        followingLabel.text = "\(people.following!.total)\nfollowing"
//        postLabel.text = "\(people.totalPhotos!)\n photos"
        aboutLabel.text = people.aboutPeople
    }
    
}
