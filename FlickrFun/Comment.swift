//
//  Comment.swift
//  FlickrFun
//
//  Created by Loka on 28/04/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

class Comment {
    
    var author: People!
    var content: String!
    var timeCreated: Int! 
}
