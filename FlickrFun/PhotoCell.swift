//
//  PhotoCell.swift
//  FlickrFun
//
//  Created by Loka on 06/05/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit
import SDWebImage

class PhotoCell: UICollectionViewCell {
    
    @IBOutlet weak var lockImageView: UIImageView!
    @IBOutlet weak var photoImageView: UIImageView!
    var photo: Photo!{
        didSet{
            updateDisplay()
        }
    }
    
    func updateDisplay(){
        photoImageView.sd_setImageWithURL(photo.getLowResUrl(), placeholderImage: UIImage.init(named: "Placeholder"))
        
        guard lockImageView != nil else{
            return
        }
        if photo.permission == Photo.permissionType.isPrivate{
            lockImageView.hidden = false
        }else {
            lockImageView.hidden = true
        }
    }
    
    func photoImageResize(){
        if self.selected{
            photoImageView.bounds.size = CGSize(width: self.bounds.width*0.9, height: self.bounds.height*0.9)
        }else{
            photoImageView.bounds.size = CGSize(width: self.bounds.width, height: self.bounds.height)
        }
    }
    
    
}
