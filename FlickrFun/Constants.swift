//
//  Constants.swift

//
//  Created by Loka on 21/04/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

class Constants{
    struct Flickr {
        static let APIScheme = "https"
        static let APIHost = "api.flickr.com"
        static let APIPath = "/services/rest/"
    }
    
    struct FlickrMethod {
        static let GetPhoto = "flickr.people.getPhotos"
        static let GetInterestingPhotoList = "flickr.interestingness.getList"
        static let SearchPhoto = "flickr.photos.search"
        static let GetComments = "flickr.photos.comments.getList"
        static let GetFavourites = "flickr.photos.getFavorites"
        static let AddComment = "flickr.photos.comments.addComment"
        static let AddFavourites = "flickr.favorites.add"
        static let RemoveFavourites = "flickr.favorites.remove"
        static let GetAlbums = "flickr.photosets.getList"
        static let GetAlbumPhotos = "flickr.photosets.getPhotos"
        static let GetFavouriteAlbum = "flickr.favorites.getList"
        static let GetGroups = "flickr.people.getGroups"
        static let GetGroupInfo = "flickr.groups.getInfo"
        static let JoinGroup = "flickr.groups.join"
        static let LeaveGroup = "flickr.groups.leave"
        static let GetFollowers = "flickr.contacts.getreverseList"
        static let GetFollowing = "flickr.contacts.getPublicList"
        static let GetUserFollowing = "flickr.contacts.getList"
        static let GetUserPublicFollowing = "flickr.contacts.getPublicList"
        static let GetPeopleInfo = "flickr.people.getInfo"
        static let SetPhotoPermission = "flickr.photos.setPerms"
        static let DeletePhoto = "flickr.photos.delete"
        static let CreateAlbum = "flickr.photosets.create"
        static let AddPhotoToAlbum = "flickr.photosets.addPhoto"
    }
    
    struct FlickrParameter {
        static let APIKey = "api_key"
        static let UserID = "user_id"
        static let Extras = "extras"
        static let Format = "format"
        static let NoJSONCallback = "nojsoncallback"
        static let Method = "method"
        static let AuthToken = "oauth_token"
        static let Text = "text"
        static let Sort = "sort"
        static let Page = "page"
        static let PhotoID = "photo_id"
        static let PerPage = "per_page"
        static let CommentText = "comment_text"
        static let AlbumID = "photoset_id"
        static let GroupID = "group_id"
        static let Friend = "friend"
        static let Family = "family"
        static let IsPublic = "is_public"
        static let IsFriend = "is_friend"
        static let IsFamily = "is_family"
        static let Title = "title"
        static let PrimaryPhotoID = "primary_photo_id"
    }
    
    struct FlickrParameterValue{
        static let APIKey = "24a6c814c5464405d6a4f08b1cf552b6"
        static let APIKeySecret = "7f48295519287dee"
        static let Extras = "date_taken, owner_name, count_comments, count_faves, isfavorite, count_contacts"
        static let Format = "json"
        static let NoJSONCallback = "1"
        static let Sort = "interestingness-desc"
    }
    
    struct FlickrResponseKey {
        static let User = "user"
        static let Id = "id"
        static let Username = "username"
        static let Title = "title"
        static let CurrentPage = "page"
        static let TotalPages = "pages"
        static let Owner = "owner"
        static let Secret = "secret"
        static let Server = "server"
        static let Farm = "farm"
        static let NumberOfFaves = "count_faves"
        static let NumberOfComments = "count_comments"
        static let DateTaken = "datetaken"
        static let OwnerName = "ownername"
    }
    
    struct Key {
        static let UserID = "userID"
        static let Username = "username"
        static let AccessToken = "accessToken"
        static let TokenSecret = "tokenSecret"
        static let Fullname = "fullname"
    }
    
}