//
//  Utility.swift
//  FlickrFun
//
//  Created by Loka on 12/05/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit


func pullDownRefresh(target: AnyObject?, selector: Selector)-> UIRefreshControl{
    
    let refreshControl = UIRefreshControl()
//    refreshControl.backgroundColor = UIColor.blueColor()
    refreshControl.attributedTitle = NSAttributedString(string: "Pull down refresh data")
    refreshControl.addTarget(target, action: selector, forControlEvents: .ValueChanged)
    return refreshControl
}

func getBackgroundLabel(containerView: UIView)-> UILabel{
    let backgroundLabelFrame = CGRectMake(0, 0, containerView.frame.width, containerView.frame.height)
    let backgroundLabel = UILabel(frame: backgroundLabelFrame)
    backgroundLabel.backgroundColor = UIColor.whiteColor()
    backgroundLabel.textAlignment = .Center
    backgroundLabel.text = "Pull Down To Refresh Data"
    return backgroundLabel
}