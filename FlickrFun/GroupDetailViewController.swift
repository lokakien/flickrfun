//
//  GroupDetailViewController.swift
//  FlickrFun
//
//  Created by Loka on 06/05/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

class GroupDetailViewController: UIViewController {
    var group: Group!
    var groupProfileCell: GroupProfileCell!
    var people: People!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        people = User.sharedInstance()
        refreshData()
        collectionView.dataSource = self
        collectionView.delegate = self
        self.navigationItem.title = group.title
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refreshData(){
        Flickr.sharedInstance().getGroupInfo(group) { groupInfo in
            //pass group info
            self.group.groupInfo = groupInfo
            self.collectionView.reloadData()
        }
    }
}


extension GroupDetailViewController: UICollectionViewDataSource{
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("groupProfileCell", forIndexPath: indexPath) as! GroupProfileCell
        cell.group = group
        groupProfileCell = cell
        return cell
    }
    
    @IBAction func dismissVC(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func joinLeaveGroup(sender: AnyObject) {
        let join = !group.isMember
        Flickr.sharedInstance().joinLeaveGroup(join, group: group) { (success, message) in
            if success == true {
                self.group.isMember = !self.group.isMember
                if self.group!.isMember == true{
                    self.people.addGroup(self.group)
                }else{
                    self.people.removeGroup(self.group)
                }
                self.groupProfileCell.setJoinLeaveButtonTitle()
            }else {
                var title: String!
                if join == true{
                    title = "Failed to join group!"
                }else {
                    title = "Failed to leave group!"
                }
                let alert = UIAlertController.init(title: title, message: message, preferredStyle: .Alert)
                let action = UIAlertAction.init(title: "OK", style: .Default, handler: nil)
                alert.addAction(action)
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }

    }
}

extension GroupDetailViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        var size: CGSize = CGSizeZero
        switch indexPath.section{
        case 0:
            size = CGSizeMake(collectionView.frame.width, collectionView.frame.height/4)
            break
        case 1:
            size = CGSizeMake(collectionView.frame.width/3, collectionView.frame.width/3)
        default:
            size = CGSizeZero
        }
        return size
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    

}
