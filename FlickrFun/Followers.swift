//
//  Followers.swift
//  FlickrFun
//
//  Created by Loka on 06/05/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

class Followers{
    var peoples: [People]!
    var total: Int!
    var page: Int!
    var totalPages: Int!
    
    init(){
        peoples = [People]()
    }
    
    func addPeople(people: People){
        peoples.append(people)
        total = total + 1
    }
    
    func removePeople(people: People){
        total = total - 1
        var index: Int!
        for i in 0...peoples.count - 1 {
            if peoples[i].userID == people.userID{
                index = i
            }
        }
        peoples.removeAtIndex(index)
    }
    
}
