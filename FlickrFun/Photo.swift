//
//  File.swift
//  FlickrOffline
//
//  Created by Loka on 21/04/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

class Photo: NSObject, NSCoding{
    var id: String!
    var title: String!
    var secret: String!
    var serverID: String!
    var farmID: String!
    var numberOfFaves: Int!
    var numberOfComments: Int!
    var dateTaken: NSDate!
    var owner: People!
    var comments: [Comment]!
//    var favourites: [People]!
    var isFavourite = false
//    var isPublic = false
//    var isFriend = false
//    var isFamily = false
    var permission: permissionType = .isPrivate
    var favourites: Favourite?
    
    enum permissionType{
        case isPublic
        case isFriend
        case isFamily
        case isPrivate
        case All
    }
    
    override init(){
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObjectForKey(Key.ID) as! String
        title = aDecoder.decodeObjectForKey(Key.Title) as! String
        //        ownerID = aDecoder.decodeObjectForKey(Key.Owner) as! String
        secret = aDecoder.decodeObjectForKey(Key.Secret) as! String
        serverID = aDecoder.decodeObjectForKey(Key.ServerID) as! String
        //        url = aDecoder.decodeObjectForKey(Key.URL) as! String
        farmID = aDecoder.decodeObjectForKey(Key.FarmID) as! String
        
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(id, forKey: Key.ID)
        aCoder.encodeObject(title, forKey: Key.Title)
        //        aCoder.encodeObject(ownerID, forKey: Key.Owner)
        aCoder.encodeObject(secret, forKey: Key.Secret)
        aCoder.encodeObject(serverID, forKey: Key.ServerID)
        //        aCoder.encodeObject(url, forKey: Key.URL)
        aCoder.encodeObject(farmID, forKey: Key.FarmID)
    }
    
    func getLowResUrl()->NSURL{
        let url = NSURL.init(string: "https://farm\(farmID).staticflickr.com/\(self.serverID)/\(self.id)_\(secret)_q.jpg")
        return url!
    }
    
    func getHighResUrl()->NSURL{
        let url = NSURL.init(string: "https://farm\(farmID).staticflickr.com/\(self.serverID)/\(self.id)_\(secret)_b.jpg")
        return url!
    }
    
    
    func addRemoveFavourite(){
        isFavourite = !isFavourite
        guard !User.sharedInstance().albums.isEmpty else{
            return
        }
        if User.sharedInstance().albums.first!.id != "faves"{
            let album = Album.init()
            album.id = "faves"
            User.sharedInstance().albums.insert(album, atIndex: 0)
        }
        let favAlbum = User.sharedInstance().albums.first!
        if isFavourite{
            numberOfFaves = numberOfFaves + 1
            favAlbum.photos.append(self)
            favAlbum.numberOfPhotos = favAlbum.numberOfPhotos + 1
        }else {
             numberOfFaves = numberOfFaves - 1
            var index: Int!
            for i in 0...favAlbum.photos.count - 1{
                if favAlbum.photos[i].id == id{
                    index = i
                }
            }
            favAlbum.photos.removeAtIndex(index)
            favAlbum.numberOfPhotos = favAlbum.numberOfPhotos - 1
        }
    }
    
    func addComment(comment: Comment){
        numberOfComments = numberOfComments + 1
        comments.append(comment)
        
    }
    
    struct Key{
        static let ID = "id"
        static let Title = "title"
        static let Owner = "owner"
        static let Secret = "secret"
        static let ServerID = "serverID"
        static let URL = "url"
        static let FarmID = "farmID"
        static let PhotoImage = "photoImage"
    }
    
    
}