//
//  User.swift
//  FlickrFun
//
//  Created by Loka on 06/05/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import Foundation
import OAuthSwift

class User: People{
    var credential: OAuthSwiftCredential?
    
    
    
    override private init() {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        credential = aDecoder.decodeObjectForKey("credential") as? OAuthSwiftCredential
    }
    
    
    override func encodeWithCoder(aCoder: NSCoder) {
        super.encodeWithCoder(aCoder)
        aCoder.encodeObject(credential, forKey: "credential")
        
    }
    
    class func sharedInstance()->User{
        struct Singleton{
            static var sharedInstance = NSKeyedUnarchiver.unarchiveObjectWithFile(User.getKeyPath()) as? User ?? User()
        }
        return Singleton.sharedInstance
    }
    
    class func getKeyPath() -> String {
        let manager = NSFileManager.defaultManager()
        let dirPath = manager.URLsForDirectory(.LibraryDirectory, inDomains: .UserDomainMask).first!
        return dirPath.URLByAppendingPathComponent("user_variable").path!
    }
    
}

