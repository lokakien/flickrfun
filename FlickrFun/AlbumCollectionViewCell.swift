//
//  AlbumCollectionViewCell.swift
//  FlickrFun
//
//  Created by Loka on 13/05/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

class AlbumCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var albumImageView: UIImageView!
    @IBOutlet weak var albumTitleLabel: UILabel!
    
    @IBOutlet weak var albumDetailLabel: UILabel!
    
    var album: Album?{
        didSet{
            albumImageView.sd_setImageWithURL(album!.albumCoverUrl, placeholderImage: UIImage.init(named: "Placeholder"))
            albumTitleLabel.text = album!.title
            albumDetailLabel.text = "\(album!.numberOfPhotos + album!.numberOfVideos) photos and videos"
        }
    }
}
