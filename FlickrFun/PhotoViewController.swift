//
//  UserDetailViewController.swift
//  FlickrOffline
//
//  Created by Loka on 21/04/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit
import SDWebImage

class PhotoViewController: UIViewController {
   
    
    @IBOutlet weak var favesButton: UIBarButtonItem!
    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var favesLabel: UILabel!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ownerNameLabel: UILabel!
    @IBOutlet weak var commentFavStackView: UIStackView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var ownerImageView: UIImageView!
    
    //    var photo: Photo!
    var photos: [Photo]!
    var selected: Int!
    var leftSwipeGesture: UISwipeGestureRecognizer!
    var rightSwipeGesture: UISwipeGestureRecognizer!
    var tapGesture: UITapGestureRecognizer!
    var commentFavTapGesture: UITapGestureRecognizer!
    var ownerNameTapGesture: UITapGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.hidden = false
        self.tabBarController?.tabBar.hidden = true
        scrollView.delegate = self
        self.navigationController?.navigationBar.topItem?.title = ""
        scrollView.autoresizingMask = [UIViewAutoresizing.FlexibleHeight, UIViewAutoresizing.FlexibleWidth]
        scrollView.maximumZoomScale = 4.0
        
    }
    
    override func viewWillAppear(animated: Bool) {
        updateDisplay()
        addGestureRecognizer()
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.view.removeGestureRecognizer(tapGesture)
        self.view.removeGestureRecognizer(leftSwipeGesture)
        self.view.removeGestureRecognizer(rightSwipeGesture)
        commentFavStackView.removeGestureRecognizer(commentFavTapGesture)
        ownerNameLabel.removeGestureRecognizer(ownerNameTapGesture)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateDisplay(){
        guard selected < photos.count else {
            return
        }
        let owner = photos[selected].owner
        self.ownerNameLabel.text = owner.fullname
        self.titleLabel.text = photos[selected].title
        self.commentsLabel.text = "\(photos[selected].numberOfComments) comments"
        self.favesLabel.text = "\(photos[selected].numberOfFaves) faves"
        ownerImageView.sd_setImageWithURL(owner.iconURL, placeholderImage: UIImage.init(named: "AccountCircle"))
        setFavesButton()
        imageView.sd_setImageWithURL(photos[selected].getLowResUrl()) { (image, error, cache, url) in
            dispatch_async(dispatch_get_main_queue(), { 
                self.imageView.sd_setImageWithURL(self.photos[self.selected].getHighResUrl())
            })
        }
        
    }
    
    func addGestureRecognizer(){
        leftSwipeGesture = UISwipeGestureRecognizer.init(target: self, action: #selector(leftSwipeDetected))
        leftSwipeGesture.direction = UISwipeGestureRecognizerDirection.Left
        rightSwipeGesture = UISwipeGestureRecognizer.init(target: self, action: #selector(rightSwipeDetected))
        rightSwipeGesture.direction = UISwipeGestureRecognizerDirection.Right
        tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapDetected))
        commentFavTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(presentToCommentVC))
        commentFavTapGesture.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tapGesture)
        self.view.addGestureRecognizer(leftSwipeGesture)
        self.view.addGestureRecognizer(rightSwipeGesture)
        commentFavStackView.addGestureRecognizer(commentFavTapGesture)
        ownerNameTapGesture = UITapGestureRecognizer(target: self, action: #selector(ownerNameTapDetected))
        ownerNameTapGesture.numberOfTapsRequired = 1
        ownerNameLabel.addGestureRecognizer(ownerNameTapGesture)
    }
    
    func leftSwipeDetected(){
        guard selected < photos.count else {
            return
        }
        selected = selected + 1
        updateDisplay()
    }
    
    func rightSwipeDetected(){
        guard selected > 0 else {
            return
        }
        selected = selected - 1
        updateDisplay()
    }
    
    func tapDetected(){
        titleLabel.hidden = !titleLabel.hidden
        closeButton.hidden = !closeButton.hidden
        toolbar.hidden = !toolbar.hidden
        favesLabel.hidden = !favesLabel.hidden
        commentsLabel.hidden = !commentsLabel.hidden
        ownerNameLabel.hidden = !ownerNameLabel.hidden
        ownerImageView.hidden = !ownerImageView.hidden
    }
    
    func ownerNameTapDetected(){
        self.performSegueWithIdentifier("seguePhotoVCToPeopleVC", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "seguePhotoVCToPeopleVC"{
            let navigationController = segue.destinationViewController as! UINavigationController
            let peopleVC = navigationController.viewControllers.first as! PeopleViewController
            peopleVC.people = self.photos[selected].owner
        }
    }
    
    
    @IBAction func dismissVC(sender: AnyObject) {
         self.dismissViewControllerAnimated(true, completion: nil)
    }
    
   
    @IBAction func commentButtonPressed(sender: AnyObject) {
        presentToCommentVC()
    }
    
   
    
    func presentToCommentVC(){
        let navigationController = self.storyboard!.instantiateViewControllerWithIdentifier("navigationControllerForCommentVC") as! UINavigationController
        let commentVC = navigationController.viewControllers.first as! CommentViewController
        commentVC.photo = photos[selected]
        self.presentViewController(navigationController, animated: true, completion: nil)
    }
    

    @IBAction func addRemoveFaves(sender: AnyObject) {
        let add = !self.photos[self.selected].isFavourite
        Flickr.sharedInstance().addRemoveFavourite(add,photoID: photos[selected].id) { (success, message) in
            if success{
                self.photos[self.selected].addRemoveFavourite()
                self.setFavesButton()
                self.updateDisplay()
            }else {
                let title: String!
                if !self.photos[self.selected].isFavourite{
                    title = "Cannot add to Favourite"
                }else {
                    title = "Cannot remove Favourite"
                }
                
                let alertController = UIAlertController.init(title:title, message: message, preferredStyle: .Alert)
                let action = UIAlertAction.init(title: "OK", style: .Default, handler: nil)
                alertController.addAction(action)
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func setFavesButton(){
        if photos[selected].isFavourite{
            favesButton.image = UIImage.init(named: "FavouriteFilled")
        }else{
            favesButton.image = UIImage.init(named: "Favourite")
        }
    }
    
    @IBAction func sharePhoto(sender: UIBarButtonItem) {
        let activityController = UIActivityViewController.init(activityItems: [imageView.image!], applicationActivities: nil)
        self.presentViewController(activityController, animated: true, completion: nil)
        
    }
    
}

extension PhotoViewController: UIScrollViewDelegate{
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
}
