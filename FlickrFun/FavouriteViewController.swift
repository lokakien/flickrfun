//
//  FavouriteViewController.swift
//  FlickrFun
//
//  Created by Loka on 29/04/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit
import MBProgressHUD

class FavouriteViewController: UIViewController {
    
    var photo:Photo!
    

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        if photo.favourites == nil {
            photo.favourites = Favourite.init()
        }
        Flickr.sharedInstance().getFavourites(photo.id, page: photo.favourites!.currentPage) { (peoples, currentPage, totalPages) in
            //pass peoples 
            self.photo.favourites!.currentPage = currentPage
            self.photo.favourites!.totalPages = totalPages
            if !peoples.isEmpty{
                self.photo.favourites!.peoples = peoples
            }
            self.tableView.reloadData()
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueFavesVCToPeopleVC"{
            let navigationController = segue.destinationViewController as! UINavigationController
            let peopleVC = navigationController.viewControllers.first as! PeopleViewController
            let indexPath = tableView.indexPathForCell(sender as! FollowCell)!
            let people = photo.favourites!.peoples[indexPath.row]
            peopleVC.people = people
        }
    }

}

extension FavouriteViewController: UITableViewDataSource{
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photo.favourites!.peoples.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("favouriteCell") as! FollowCell
        cell.people = photo.favourites!.peoples[indexPath.row]
        cell.delegate = self
        return cell
    }
}



extension FavouriteViewController: FollowCellDelegate{
    func followCell(followCell: FollowCell, didTapOnFollowButton: Bool) {
        let indexPath = tableView.indexPathForCell(followCell)!
        let people = photo.favourites!.peoples[indexPath.row]
        let isFollowing = followCell.checkIfFollowing(people)
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        if isFollowing{
            Flickr.sharedInstance().removeFollowing(people, completionHandler: { (success) in
                if success{
                    User.sharedInstance().following!.removePeople(people)
                    followCell.updateFollowButton()
                }
                MBProgressHUD.hideHUDForView(self.view, animated: true)
            })
        }else{
            Flickr.sharedInstance().addFollowing(people, relationship: People.relationType.publicPerson , completionHandler: { (success) in
                if success{
                    User.sharedInstance().following!.addPeople(people)
                    followCell.updateFollowButton()
                    
                }
                MBProgressHUD.hideHUDForView(self.view, animated: true)
            })
        }
        
    }
    
    func followCell(followCell: FollowCell, didTapOnIconImageView: Bool){
        self.performSegueWithIdentifier("segueFavesVCToPeopleVC", sender: followCell)
    }
    

    

    

}
