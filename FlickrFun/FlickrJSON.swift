//
//  FlickrJSON.swift
//  FlickrFun
//
//  Created by Loka on 06/05/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import Foundation


extension Flickr{
    
    func getParsedJSONToFollowers(parsedJSON: [String:AnyObject])-> Followers{
        let followers = Followers()
        guard let contactsJSON = parsedJSON["contacts"] as? [String: AnyObject] else{
            print ("cant parse JSON")
            return followers
        }
        followers.page = contactsJSON["page"] as! Int
        followers.totalPages = contactsJSON["pages"] as! Int
        followers.total = contactsJSON["total"] as? Int
        if followers.total == nil! {
            followers.total = Int(contactsJSON["total"] as! String)
        }
        let contactJSONArray = contactsJSON["contact"] as! [[String: AnyObject]]
        for contactJSON in contactJSONArray{
            let people = People()
            people.userID = contactJSON["nsid"] as! String
            people.fullname = contactJSON["realname"] as? String
            people.username = contactJSON["username"] as? String
            let friend = contactJSON["friend"] as? Int
            let family = contactJSON["family"] as? Int
            if friend == 1{
                people.relationship = People.relationType.friend
            }
            if family == 1 {
                people.relationship = People.relationType.family
            }
            if family == 0 && friend == 0 {
                people.relationship = People.relationType.publicPerson
            }
            followers.peoples.append(people)
        }
        
        return followers
    }
    
    func getParsedJSONToPeopleInfo(parsedJSON: [String: AnyObject], people: People){
        guard let personJSON = parsedJSON["person"] as? [String: AnyObject] else{
            print ("cant parse JSON")
            return
        }
        let descriptionJSON = personJSON["description"] as! [String: AnyObject]
        people.aboutPeople = descriptionJSON["_content"] as? String
        let locationJSON = personJSON["location"] as? [String: AnyObject]
        if locationJSON != nil {
            people.location = locationJSON!["_content"] as? String
        }
        let photoJSON = personJSON["photos"] as! [String: AnyObject]
        let photoCountJSON = photoJSON["count"] as! [String: AnyObject]
        people.totalPhotos = photoCountJSON["_content"] as? Int
    }
    
    
    func getParsedJSONToPhotoArray(parsedJSON: [String: AnyObject]) -> ([Photo], currentPage: Int, totalPages: Int){
        var photos = [Photo]()
        guard let photosJSON = parsedJSON["photos"] as? [String: AnyObject] else{
            print ("No photos arg")
            return (photos, 0, 0)
        }
        
        let totalPages = photosJSON["pages"] as! Int
        let currentPage = photosJSON["page"] as! Int
        
        print ("\(totalPages) \(currentPage)")
        
        guard let photoArrayJSON = photosJSON["photo"] as? [[String: AnyObject]] else{
            print("no photo array arg")
            return (photos, 0, 0)
        }
        
        for photoJSON in photoArrayJSON{
            let photo = Photo()
            photo.id = photoJSON[Constants.FlickrResponseKey.Id] as! String
            photo.title = photoJSON[Constants.FlickrResponseKey.Title] as! String
            let ownerID = photoJSON[Constants.FlickrResponseKey.Owner] as! String
            photo.secret = photoJSON[Constants.FlickrResponseKey.Secret] as! String
            photo.serverID  = photoJSON[Constants.FlickrResponseKey.Server] as! String
            photo.farmID  = String(photoJSON[Constants.FlickrResponseKey.Farm] as! Int)
            photo.numberOfFaves = Int(photoJSON[Constants.FlickrResponseKey.NumberOfFaves] as! String)
            photo.numberOfComments = Int(photoJSON[Constants.FlickrResponseKey.NumberOfComments] as! String)
            let dateTakenString = photoJSON[Constants.FlickrResponseKey.DateTaken] as! String
            let dateFormatter = NSDateFormatter.init()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            photo.dateTaken = dateFormatter.dateFromString(dateTakenString)
            let ownerName = photoJSON[Constants.FlickrResponseKey.OwnerName] as! String
            let isFavorite = photoJSON["isfavorite"] as! Int
            let isPublic = photoJSON["ispublic"] as! Int
            let isFriend = photoJSON["isfriend"] as! Int
            let isFamily = photoJSON["isfamily"] as! Int
            let owner = People.init()
            owner.fullname = ownerName
            owner.userID = ownerID
            photo.owner = owner
            if isFavorite == 1{
                photo.isFavourite = true
            }
            if isFamily == 1 {
                photo.permission = .isFamily
            }
            if isPublic == 1{
                photo.permission = .isPublic
            }
            if isFriend == 1 {
                photo.permission = .isFriend
            }
            photos.append(photo)
        }
        return (photos, currentPage, totalPages)
    }
    
    func getParsedJSONToCommentArray(parsedJSON: [String: AnyObject])-> [Comment]{
        var comments = [Comment]()
        guard let commentsJSON = parsedJSON["comments"] as? [String: AnyObject] else {
            print ("unable to parse JSON")
            return comments
        }
        
        guard let commentJSONArray = commentsJSON["comment"] as? [[String: AnyObject]] else {
            print ("No comment array")
            return comments
        }
        
        for commentJSON in commentJSONArray{
            let authorID = commentJSON["author"] as! String
            let timeCreated = Int(commentJSON["datecreate"] as! String)!
            let authorName = commentJSON["authorname"] as! String
            let realName = commentJSON["realname"] as! String
            let content = commentJSON["_content"] as! String
            
            let author = People.init()
            author.userID = authorID
            author.username = authorName
            author.fullname = realName
            let comment = Comment()
            comment.author = author
            comment.content = content
            comment.timeCreated = timeCreated
            comments.append(comment)
        }
        
        return comments
    }
    
    func getParsedJSONtoComment(parsedJSON: [String: AnyObject] )-> Comment?{
        guard let commentJSON = parsedJSON["comment"] as? [String: AnyObject] else {
            return nil
        }
        
        let authorID = commentJSON["author"] as! String
        let authorFullname = commentJSON["realname"] as! String
        let authorUsername = commentJSON["authorname"] as! String
        let content = commentJSON["_content"] as! String
        let timeCreated = Int(commentJSON["datecreate"] as! String)!
        let people = People.init()
        people.userID = authorID
        people.fullname = authorFullname
        people.username = authorUsername
        let comment = Comment.init()
        comment.author = people
        comment.content = content
        comment.timeCreated = timeCreated
        
        print (comment.content)
        return comment
    }
    
    func getParsedJSONToFavouriteArray(parsedJSON: [String: AnyObject])->([People],currentPage:Int, totalPages:Int){
        var peoples = [People]()
        guard let photoJSON = parsedJSON["photo"] as? [String: AnyObject] else {
            print ("unable to parse JSON")
            return (peoples,0, 0)
        }
        
        let currentPage = photoJSON["page"] as! Int
        let totalPages = photoJSON["pages"] as! Int
        guard let personJSONArray = photoJSON["person"] as? [[String: AnyObject]] else {
            print ("unable to parse JSON")
            return (peoples,0, 0)
        }
        for personJSON in personJSONArray{
            let userID = personJSON["nsid"] as! String
            let username = personJSON["username"] as! String
            let realname = personJSON["realname"] as! String
            let people = People()
            people.userID = userID
            people.username = username
            people.fullname = realname
            peoples.append(people)
        }
        return (peoples,totalPages, currentPage)
    }
    
    
    func getParsedJSONtoAlbums(parsedJSON: [String: AnyObject])->(albums: [Album], currentPage: Int, totalPages: Int){
        var albums = [Album]()
        guard let photosetsJSON = parsedJSON["photosets"] as? [String: AnyObject] else {
            print ("unable to parse JSON")
            return (albums, 0, 0)
        }
        
        var currentPage = photosetsJSON["page"] as? Int
        if currentPage == nil{
            currentPage = Int(photosetsJSON["page"] as! String)
        }
        let totalPages = photosetsJSON["pages"] as! Int
        
        guard let photosetJSONArray = photosetsJSON["photoset"] as? [[String: AnyObject]] else{
            print ("no photoset")
            return (albums,currentPage!, totalPages)
        }
        
        for photosetJSON in photosetJSONArray{
            let album = Album.init()
            let titleJSON = photosetJSON["title"] as! [String: AnyObject]
            album.title = titleJSON["_content"] as! String
            var numberOfPhotos = photosetJSON["photos"] as? Int
            if numberOfPhotos == nil {
                numberOfPhotos = Int(photosetJSON["photos"] as! String)
            }
            album.numberOfPhotos = numberOfPhotos!
            var numberOfVideos = photosetJSON["videos"] as? Int
            if numberOfVideos == nil{
                   numberOfVideos = Int(photosetJSON["videos"] as! String)!
            }
            album.numberOfVideos = numberOfVideos!
            album.primaryFarmID = photosetJSON["farm"] as! Int
            album.primaryID = photosetJSON["primary"] as! String
            album.id = photosetJSON["id"] as! String
            album.primaryServerID = photosetJSON["server"] as! String
            album.primarySecret = photosetJSON["secret"] as! String
            albums.append(album)
            
        }
        //        print (photosetJSONArray)
        return (albums,currentPage!, totalPages)

    }
    
    func getParsedJSONToFavAlbum(parsedJSON: [String: AnyObject]) -> Album? {
        guard let photosJSON = parsedJSON["photos"] else {
            return nil
        }
        let photoJSONArray = photosJSON["photo"] as! [[String: AnyObject]]
        guard photoJSONArray.count > 0 else {
            return nil
        }
        let photoJSON  = photoJSONArray[0]
        let album = Album()
        album.title = "Faves"
        album.numberOfPhotos = photoJSONArray.count
        album.primaryID = photoJSON["id"] as! String
        album.primaryFarmID = photoJSON["farm"] as! Int
        album.id = "faves"
        album.primaryServerID = photoJSON["server"] as! String
        album.primarySecret = photoJSON["secret"] as! String
        
        return album
    }
    
    func getParsedJSONfromPhotosetToPhotoArray(parsedJSON: [String: AnyObject], owner: People)-> ([Photo],currentPage: Int,totalPages: Int){
        var photos = [Photo]()
        guard let photosetJSON = parsedJSON["photoset"] as? [String: AnyObject] else{
            print ("cant parse JSON")
            return (photos, 0, 0)
        }
        
        var currentPage = photosetJSON["page"] as? Int
        if currentPage == nil {
            currentPage = Int((photosetJSON["page"] as! String))
        }
        let totalPages = photosetJSON["pages"] as! Int
        guard let photoJSONArray = photosetJSON["photo"] as? [[String: AnyObject]] else {
            return (photos, currentPage!, totalPages)
        }
        for photoJSON in photoJSONArray{
            let photo = Photo()
            photo.id = photoJSON["id"] as! String
            photo.title = photoJSON["title"] as! String
            photo.secret = photoJSON["secret"] as! String
            photo.farmID = String(photoJSON["farm"] as! Int)
            photo.serverID = photoJSON["server"] as! String
            photo.numberOfFaves = Int(photoJSON["count_faves"] as! String)
            photo.numberOfComments = Int(photoJSON["count_comments"] as! String)
            let dateTakenString = photoJSON["datetaken"] as! String
            let dateFormatter = NSDateFormatter.init()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dateTaken = dateFormatter.dateFromString(dateTakenString)
            photo.dateTaken = dateTaken
            let isFavorite = photoJSON["isfavorite"] as! Int
            let isPublic = photoJSON["ispublic"] as! Int
            let isFriend = photoJSON["isfriend"] as! Int
            let isFamily = photoJSON["isfamily"] as! Int
            if isFavorite == 1{
                photo.isFavourite = true
            }
            if isFamily == 1 {
                photo.permission = Photo.permissionType.isFamily
            }
            if isPublic == 1{
                photo.permission = Photo.permissionType.isPublic
            }
            if isFriend == 1 {
                photo.permission = Photo.permissionType.isFriend
            }
            photo.owner = owner
            photos.append(photo)
        }
        return (photos, currentPage!, totalPages)
    }
    
    func getParsedJSONToGroup(parsedJSON: [String: AnyObject])-> [Group]{
        var groups = [Group]()
        guard let groupsJSON = parsedJSON["groups"] as? [String: AnyObject] else {
            print ("cant parse JSON")
            return groups
        }
        
        guard let groupJSONArray = groupsJSON["group"] as? [[String: AnyObject]] else{
            print ("do not have group JSON")
            return groups
        }
        for groupJSON in groupJSONArray{
            let group = Group()
            group.title = groupJSON["name"] as! String
            group.id = groupJSON["nsid"] as! String
            group.numberOfGroupMembers = Int(groupJSON["members"] as! String)
            group.numberOfPhotos = Int(groupJSON["pool_count"] as! String)
            let isMember = groupJSON["is_member"] as! Int
            if isMember == 1{
                group.isMember = true
            }
            let invitationOnly = groupJSON["invitation_only"] as! Int
            if invitationOnly == 1{
                group.invitationOnly = true
            }
            groups.append(group)
        }
        
        return groups
    }
    

    func getParsedJSONToGroupForUser(parsedJSON: [String: AnyObject], people: People)-> [Group]{
        var groups = [Group]()
        guard let groupsJSON = parsedJSON["groups"] as? [String: AnyObject] else {
            print ("cant parse JSON")
            return groups
        }
        
        guard let groupJSONArray = groupsJSON["group"] as? [[String: AnyObject]] else{
            print ("do not have group JSON")
            return groups
        }
        for groupJSON in groupJSONArray{
            let group = Group()
            group.title = groupJSON["name"] as! String
            group.id = groupJSON["nsid"] as! String
            group.numberOfGroupMembers = Int(groupJSON["members"] as! String)
            group.numberOfPhotos = Int(groupJSON["pool_count"] as! String)
//            var isMember = groupJSON["is_member"] as! Int
//            if isMember == 1{
//                group.isMember = true
//            }
            let filteredGroup = people.groups.filter{$0.id == group.id}
            if !filteredGroup.isEmpty{
                group.isMember = true
            }
            let invitationOnly = groupJSON["invitation_only"] as! Int
            if invitationOnly == 1{
                group.invitationOnly = true
            }
            groups.append(group)
        }
        
        return groups
    }

    
    func getParsedJSONToGroupInfo(parsedJSON: [String: AnyObject])->GroupInfo?{
        guard let groupJSON = parsedJSON["group"] as? [String: AnyObject] else {
            print ("Cant parse JSON")
            return nil
        }
        let groupInfo = GroupInfo()
        let descriptionJSON = groupJSON["description"] as! [String: AnyObject]
        groupInfo.description = descriptionJSON["_content"] as! String
        let rulesJSON = groupJSON["rules"] as! [String: AnyObject]
        groupInfo.rules = rulesJSON["_content"] as! String
        let topicJSON = groupJSON["topic_count"] as! [String: AnyObject]
        groupInfo.topicCount = Int(topicJSON["_content"] as! String)
        let privacyJSON = groupJSON["privacy"] as! [String: AnyObject]
        groupInfo.privacy = Int(privacyJSON["_content"] as! String)
        return groupInfo
    }
    
    
    
    func joinLeaveGroup(join: Bool, group: Group, completionHandler: (success: Bool, message: String?)-> ()) {
        let extraParameters = [Constants.FlickrParameter.GroupID: group.id!]
        var method: String!
        if join == true {
            method = Constants.FlickrMethod.JoinGroup
        }else {
            method = Constants.FlickrMethod.LeaveGroup
        }
        
        FlickrClient.sharedInstance().FlickrPostMethod(method, extraParameter: extraParameters, successCompletionHandler: { (parsedJSON) in
            let status = parsedJSON["stat"] as! String
            var success = false
            if  status == "ok"{
                success = true
            }
            let message = parsedJSON["message"] as? String
            completionHandler(success: success, message: message)
            }, failureHandler: {
                print ("join group failed")
        })
    }
    
    func getGroupInfo(group: Group, completionHandler: GroupInfo?->()){
        let extraParameters = [Constants.FlickrParameter.GroupID: group.id!]
        
        FlickrClient.sharedInstance().FlickrGetMethod(Constants.FlickrMethod.GetGroupInfo, extraParameter: extraParameters, successCompletionHandler: { (parsedJSON) in
            let groupInfo: GroupInfo? = self.getParsedJSONToGroupInfo(parsedJSON)
            completionHandler(groupInfo)
            }, failCompletionHandler: {
                print ("can't get group info")
        })
    }





    

}