//
//  People.swift
//  FlickrFun
//
//  Created by Loka on 06/05/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

class People: NSObject, NSCoding{
    var username: String?
    var fullname: String?
    var userID: String!
    var photos: [Photo] = []
    var totalPhotos: Int?
    var iconURL: NSURL!{
        get {
            return NSURL(string: "https://flickr.com/buddyicons/\(userID).jpg")!
        }
    }
    var followers: Followers?
    var following: Followers?
    var relationship: relationType?
    var location: String?
    var aboutPeople: String?
    var albums: [Album] = []
    var albumsCurrentPage: Int = 1
    var albumsTotalPages: Int = 2
    var groups: [Group] = []
    
    enum relationType{
        case family
        case friend
        case publicPerson
        case ownSelf
    }
    
    
    override init(){
        super.init()
        
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init()
        username = aDecoder.decodeObjectForKey(Constants.Key.Username) as? String
        userID = aDecoder.decodeObjectForKey(Constants.Key.UserID) as? String
        fullname = aDecoder.decodeObjectForKey(Constants.Key.Fullname) as? String
        totalPhotos = aDecoder.decodeObjectForKey("totalPhotos") as? Int
        aboutPeople = aDecoder.decodeObjectForKey("aboutPeople") as? String
    }
    
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(username, forKey: Constants.Key.Username)
        aCoder.encodeObject(fullname, forKey: Constants.Key.Fullname)
        aCoder.encodeObject(userID, forKey: Constants.Key.UserID)
        aCoder.encodeObject(totalPhotos, forKey: "totalPhotos")
        aCoder.encodeObject(aboutPeople, forKey: "aboutPeople")
    }
    
    func addGroup(group: Group){
        groups.append(group)
    }
    
    func removeGroup(group: Group){
        var index: Int!
        for i in 0...groups.count - 1{
            if groups[i].id == group.id{
                index = i
            }
        }
        groups.removeAtIndex(index)
    }
    
}
