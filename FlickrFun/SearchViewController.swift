//
//  SearchViewController.swift
//  FlickrOffline
//
//  Created by Loka on 21/04/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit
import SDWebImage
import MBProgressHUD

class SearchViewController: UIViewController {
    var photos: [Photo]?
    var currentPage: Int = 1
    var totalPages: Int = 2
    var tapGestureRecognizer: UITapGestureRecognizer?
    var rightSwipe = UISwipeGestureRecognizer()
    var method = Constants.FlickrMethod.GetInterestingPhotoList
    var refreshControl: UIRefreshControl?
    var tapOutsideKeyboardGesture: UITapGestureRecognizer?
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!


    override func viewDidLoad() {
        super.viewDidLoad()
                collectionView.dataSource = self
        collectionView.delegate = self
        searchBar.delegate = self
        refreshData()
        collectionView.alwaysBounceVertical = true
        refreshControl = pullDownRefresh(self, selector: #selector(refreshData))
        collectionView.addSubview(refreshControl!)
        let bottomLabelFrame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + self.view.frame.height - 40 - self.tabBarController!.tabBar.frame.height, self.view.frame.width, 40)
        let bottomLabel = UILabel(frame: bottomLabelFrame)
        bottomLabel.text = "Pull up for more"
        bottomLabel.backgroundColor = UIColor.purpleColor()
//        self.view.addSubview(bottomLabel)
        
    }
    
    
    func refreshData(){
        Flickr.sharedInstance().getInterestingPhotosOfDay(1,totalPages: totalPages){ (photos, currentPage, totalPages) in
            self.currentPage = currentPage
            self.totalPages = totalPages
//            if self.photos == nil {
//                self.photos = photos
//            }else{
//                for photo in photos{
//                    self.photos!.append(photo)
//                }
//            }
            self.photos = photos
            self.refreshControl?.endRefreshing()
            self.collectionView.reloadData()
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        rightSwipe.direction = UISwipeGestureRecognizerDirection.Right
        rightSwipe.addTarget(self, action: #selector(SearchViewController.swiped))
        self.view.addGestureRecognizer(rightSwipe)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillShow), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillHide), name: UIKeyboardWillHideNotification, object: nil)

    }
    
    override func viewDidAppear(animated: Bool) {
    }
    
    override func viewDidDisappear(animated: Bool) {
        self.view.removeGestureRecognizer(rightSwipe)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: self)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func swiped(swipe: UISwipeGestureRecognizer){
        self.tabBarController!.selectedIndex = self.tabBarController!.selectedIndex - 1
    }
    
    func keyboardWillShow(){
        tapOutsideKeyboardGesture = UITapGestureRecognizer(target: self, action: #selector(tapOutsideKeyboard))
        self.collectionView.addGestureRecognizer(tapOutsideKeyboardGesture!)
    }
    
    func keyboardWillHide(){
        if tapOutsideKeyboardGesture != nil{
            self.collectionView.removeGestureRecognizer(tapOutsideKeyboardGesture!)
        }
    }
    
    func tapOutsideKeyboard(){
        if self.searchBar.isFirstResponder(){
            self.searchBar.resignFirstResponder()
        }
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        if collectionView != nil {
            collectionView.collectionViewLayout.invalidateLayout()
        }
    }
}

extension SearchViewController: UICollectionViewDataSource{
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let count: Int!
        if photos == nil {
            count = 0
        }else{
            count = photos!.count
        }
        if count == 0{
            self.collectionView.backgroundView = getBackgroundLabel(collectionView)
        }
        return count
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("photoCell", forIndexPath: indexPath) as! PhotoCell
        guard let photos = self.photos else{
            return cell
        }
        cell.photo = photos[indexPath.item]
        if indexPath.item + 6 < photos.count {
            SDWebImagePrefetcher.sharedImagePrefetcher().prefetchURLs([photos[indexPath.item + 6].getLowResUrl()])
        }
        
        return cell
    }
}

extension SearchViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        var numberOfRows: Int = 0
        if collectionView.frame.width < collectionView.frame.height {
            numberOfRows = 2
        }else {
            numberOfRows = 3
        }
        let cellWidth = collectionView.frame.width / CGFloat(numberOfRows)
        let size = CGSizeMake(cellWidth, cellWidth)
        return size
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
}


extension SearchViewController: UICollectionViewDelegate{
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("segueSearchVCToPhotoVC", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueSearchVCToPhotoVC" {
            let photoVC = segue.destinationViewController as! PhotoViewController
            let indexPath = collectionView.indexPathsForSelectedItems()![0]
            photoVC.photos = self.photos!
            photoVC.selected = indexPath.item
        }
    }
}

extension SearchViewController: UISearchBarDelegate{
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        Flickr.sharedInstance().searchPhoto(searchBar.text!, page: 1, totalPages: totalPages) { (photos, currentPage, totalPages) in
            self.currentPage  = currentPage
            self.totalPages = totalPages
            self.photos = photos
            self.collectionView.reloadData()
            if self.collectionView.numberOfItemsInSection(0) > 0{
                let indexPath = NSIndexPath(forItem: 0, inSection: 0)
                self.collectionView.scrollToItemAtIndexPath(indexPath, atScrollPosition: .Top, animated: true)
            }
        }
        method = Constants.FlickrMethod.SearchPhoto
        searchBar.resignFirstResponder()
    }
    
    func scrollViewWillEndDragging(scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        pullUpToLoadMoreData(method, scrollView: scrollView)
    }
    
    func pullUpToLoadMoreData(method: String, scrollView: UIScrollView){
        let scrollOffsetY = scrollView.contentOffset.y
        let triggerOffset: CGFloat = 20.0
        guard scrollOffsetY - triggerOffset > scrollView.contentSize.height - scrollView.frame.size.height else {
            return
        }
        if currentPage < totalPages {
            self.collectionView.userInteractionEnabled = false
//            self.view.frame.origin.y = self.view.frame.origin.y - 40
            print("current page : \(currentPage)")
            MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            if method == Constants.FlickrMethod.GetInterestingPhotoList{
                Flickr.sharedInstance().getInterestingPhotosOfDay(currentPage + 1, totalPages: totalPages){ (photos, currentPage, totalPages) in
                    MBProgressHUD.hideHUDForView(self.view, animated: true)
                    self.handlePullUpToLoadResponse(photos, currentPage: currentPage, totalPages: totalPages)
                }
            }
            if method == Constants.FlickrMethod.SearchPhoto{
                Flickr.sharedInstance().searchPhoto(searchBar.text!, page: currentPage + 1, totalPages: totalPages, completionHandler: { (photos, currentPage, totalPages) in
                    MBProgressHUD.hideHUDForView(self.view, animated: true)
                    self.handlePullUpToLoadResponse(photos, currentPage: currentPage, totalPages: totalPages)
                })
            }
            
        }
        
    }
    
    func handlePullUpToLoadResponse(photos: [Photo], currentPage: Int, totalPages: Int) {
        for photo in photos{
            self.photos!.append(photo)
        }
        self.currentPage = currentPage
        self.totalPages = totalPages
        let mainQueue = dispatch_get_main_queue()
        dispatch_async(mainQueue, {
            self.collectionView.reloadData()
            self.collectionView.userInteractionEnabled = true
        })
    }


}



