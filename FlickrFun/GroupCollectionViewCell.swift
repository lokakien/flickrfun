//
//  GroupCollectionViewCell.swift
//  FlickrFun
//
//  Created by Loka on 13/05/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

class GroupCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var groupImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var memberLabel: UILabel!
    @IBOutlet weak var photoLabel: UILabel!
    
    var group: Group!{
        didSet{
            groupImageView.sd_setImageWithURL(group.coverPhotoURL, placeholderImage: UIImage.init(named: "Placeholder"))
            titleLabel.text = group.title
            memberLabel.text = "\(group.numberOfGroupMembers) members"
            photoLabel.text = "\(group.numberOfPhotos) photos"
        }
    }

}
