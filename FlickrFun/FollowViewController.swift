//
//  FollowViewController.swift
//  FlickrFun
//
//  Created by Loka on 06/05/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit
import MBProgressHUD

class FollowViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var followers: Followers!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        self.navigationItem.backBarButtonItem?.title = ""
    
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

extension FollowViewController: UITableViewDataSource{
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return followers.peoples.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("followCell", forIndexPath: indexPath) as! FollowCell
        cell.people = followers.peoples[indexPath.row]
        cell.delegate = self
        return cell
    }
}

extension FollowViewController: UITableViewDelegate{
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("segueFollowVCToPeopleVC", sender: self)
//        let navigationController = self.storyboard?.instantiateViewControllerWithIdentifier("peopleNavigationController") as! UINavigationController
//        let peopleProfileVC = navigationController.viewControllers.first as! PeopleViewController
//        peopleProfileVC.people = followers.peoples[indexPath.row]
//        self.presentViewController(navigationController, animated: true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueFollowVCToPeopleVC"{
            let navigationController = segue.destinationViewController as! UINavigationController
            let peopleVC  = navigationController.viewControllers.first as! PeopleViewController
            let indexPath = tableView.indexPathForSelectedRow!
            peopleVC.people = followers.peoples[indexPath.row]
        
        }
    }
    
}

extension FollowViewController: FollowCellDelegate{
    func followCell(followCell: FollowCell, didTapOnFollowButton: Bool) {
        let indexPath = tableView.indexPathForCell(followCell)!
        let people = followers.peoples[indexPath.row]
        let isFollowing = checkIfFollowing(people)
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        if isFollowing{
            Flickr.sharedInstance().removeFollowing(people, completionHandler: { (success) in
                if success{
                    User.sharedInstance().following!.removePeople(people)
                    followCell.updateFollowButton()
                }
                MBProgressHUD.hideHUDForView(self.view, animated: true)
            })
        }else{
            Flickr.sharedInstance().addFollowing(people, relationship: People.relationType.publicPerson , completionHandler: { (success) in
                if success{
                    User.sharedInstance().following!.addPeople(people)
                    followCell.updateFollowButton()
                    
                }
                MBProgressHUD.hideHUDForView(self.view, animated: true)
            })
        }
        
    }
    
    func followCell(followCell: FollowCell, didTapOnIconImageView: Bool){
        
    }
    
    func checkIfFollowing(people: People)->Bool{
        let filterArray = User.sharedInstance().following!.peoples.filter{$0.userID == people.userID}
        if !filterArray.isEmpty{
            return true
        }else{
            return false
        }
    }
    
 

    
    
}