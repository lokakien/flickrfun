//
//  CommentCell.swift
//  FlickrFun
//
//  Created by Loka on 28/04/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

protocol CommentCellDelegate{
    func commentCell(commentCell: CommentCell, didTapOnIconImageView: Bool)
}

class CommentCell: UITableViewCell {


    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!{
        didSet{
            iconImageView.userInteractionEnabled = true
            iconImageViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(tapOnIConImage))
            iconImageView.addGestureRecognizer(iconImageViewTapGesture)
        }
    }
    
    var delegate: CommentCellDelegate!
    var iconImageViewTapGesture: UITapGestureRecognizer!
    
    var comment: Comment!{
        didSet{
            commentLabel.text = comment.content
            timeLabel.text = String(comment.timeCreated)
            let author = comment.author
            if author.fullname == "" || author.fullname == nil {
                nameLabel.text = author.username
            }else{
                nameLabel.text = author.fullname
            }
            iconImageView.sd_setImageWithURL(author.iconURL, placeholderImage: UIImage.init(named: "AccountCircleBlack"))
            
        }
    }
    
    func tapOnIConImage(){
        self.delegate.commentCell(self, didTapOnIconImageView: true)
    }
}
