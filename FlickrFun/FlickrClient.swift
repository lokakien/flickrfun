//
//  FlickrClient.swift
//  FlickrOffline
//
//  Created by Loka on 21/04/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit
import OAuthSwift


class FlickrClient{
    
    class func sharedInstance()->FlickrClient{
        struct Singleton{
            static var sharedInstance = FlickrClient()
        }
        return Singleton.sharedInstance
    }
    
    func URLFromParameters(parameters: [String:AnyObject]) -> NSURL {
        
        let components = NSURLComponents()
        components.scheme = Constants.Flickr.APIScheme
        components.host = Constants.Flickr.APIHost
        components.path = Constants.Flickr.APIPath
        components.queryItems = [NSURLQueryItem]()
        
        for (key, value) in parameters {
            let queryItem = NSURLQueryItem(name: key, value: "\(value)")
            components.queryItems!.append(queryItem)
        }
        
        return components.URL!
    }
    
    func getImage(url: NSURL, onImageLoaded:((image: UIImage?, url: NSURL) -> Void)? ) {
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithURL(url) { (data, response, error) in
            //no error handling yet
            if let data = data {
                let image = UIImage(data: data)
                onImageLoaded!(image: image, url: url)
            }
            
        }
        task.resume()
    }
    
    
    func FlickrPostMethod (method: String, extraParameter: [String: String]?, successCompletionHandler: (parsedJSON: [String: AnyObject])->(), failureHandler: ()-> ()) {
        let oauthswift = OAuth1Swift(
            consumerKey:    Constants.FlickrParameterValue.APIKey,
            consumerSecret: Constants.FlickrParameterValue.APIKeySecret ,
            requestTokenUrl: "https://www.flickr.com/services/oauth/request_token",
            authorizeUrl:    "https://www.flickr.com/services/oauth/authorize",
            accessTokenUrl:  "https://www.flickr.com/services/oauth/access_token"
        )
        let url :String = "https://api.flickr.com/services/rest/"
        var parameters : [String: String] = [
            Constants.FlickrParameter.Method         : method,
            Constants.FlickrParameter.APIKey         : Constants.FlickrParameterValue.APIKey,
            Constants.FlickrParameter.Format         : Constants.FlickrParameterValue.Format,
            Constants.FlickrParameter.NoJSONCallback : Constants.FlickrParameterValue.NoJSONCallback]
        
        if extraParameter != nil{
            for (key,value) in extraParameter!{
                parameters[key] = value
            }
        }
        
        let headers =  ["Accept": "application/json", "Content-Type":"application/x-www-form-urlencoded"]
        oauthswift.client.credential.oauth_token = User.sharedInstance().credential!.oauth_token
        oauthswift.client.credential.oauth_token_secret = User.sharedInstance().credential!.oauth_token_secret
        oauthswift.client.post(url, parameters: parameters, headers: headers, success: { (data, response) in
            //            let data = dataString!.dataUsingEncoding(NSUTF8StringEncoding)!
            let parsedJSON = try? NSJSONSerialization.JSONObjectWithData(data, options: []) as! [String: AnyObject]
            successCompletionHandler(parsedJSON: parsedJSON!)
            }, failure: { error in
                print (error)
                failureHandler()
                
        })
    }
    
    func FlickrGetMethod(method: String, extraParameter: [String: String]?, successCompletionHandler: (parsedJSON: [String: AnyObject])->(), failCompletionHandler: ()->()) {
        let oauthswift = OAuth1Swift(
            consumerKey:    Constants.FlickrParameterValue.APIKey,
            consumerSecret: Constants.FlickrParameterValue.APIKeySecret ,
            requestTokenUrl: "https://www.flickr.com/services/oauth/request_token",
            authorizeUrl:    "https://www.flickr.com/services/oauth/authorize",
            accessTokenUrl:  "https://www.flickr.com/services/oauth/access_token"
        )
        oauthswift.client.credential.oauth_token = User.sharedInstance().credential!.oauth_token
        oauthswift.client.credential.oauth_token_secret = User.sharedInstance().credential!.oauth_token_secret
        let url :String = "https://api.flickr.com/services/rest/"
        var parameters :Dictionary = [
            Constants.FlickrParameter.Method         : method,
            Constants.FlickrParameter.APIKey         : Constants.FlickrParameterValue.APIKey,
            Constants.FlickrParameter.Format         : Constants.FlickrParameterValue.Format,
            Constants.FlickrParameter.NoJSONCallback : Constants.FlickrParameterValue.NoJSONCallback,
            Constants.FlickrParameter.Extras         : Constants.FlickrParameterValue.Extras,
            ]
        
        if extraParameter != nil{
            for (key,value) in extraParameter!{
                parameters[key] = value
            }
        }
        oauthswift.client.get(url, parameters: parameters,
                              success: {
                                data, response in
                                let dataString = NSString.init(data: data, encoding: NSUTF8StringEncoding)
                                let data = dataString!.dataUsingEncoding(NSUTF8StringEncoding)!
                                let parsedJSON = try? NSJSONSerialization.JSONObjectWithData(data, options: []) as! [String: AnyObject]
                                successCompletionHandler(parsedJSON: parsedJSON!)
            }, failure: { error in
                failCompletionHandler()
        })
    }
    
    func doOAuthFlickr(completionHandler: (success: Bool)-> ()){
        let oauthswift = OAuth1Swift(
            consumerKey:    Constants.FlickrParameterValue.APIKey,
            consumerSecret: Constants.FlickrParameterValue.APIKeySecret ,
            requestTokenUrl: "https://www.flickr.com/services/oauth/request_token",
            authorizeUrl:    "https://www.flickr.com/services/oauth/authorize",
            accessTokenUrl:  "https://www.flickr.com/services/oauth/access_token"
        )
        oauthswift.authorizeWithCallbackURL(
            NSURL(string: "flickrfun://oauth-callback/flickr")!,
            success: { credential, response, parameters in
                User.sharedInstance().credential = credential
                User.sharedInstance().fullname = parameters["fullname"]?.stringByRemovingPercentEncoding!
                User.sharedInstance().username = parameters["username"]?.stringByRemovingPercentEncoding!
                User.sharedInstance().userID = parameters["user_nsid"]?.stringByRemovingPercentEncoding!
                
                NSKeyedArchiver.archiveRootObject(User.sharedInstance(), toFile: User.getKeyPath())
                completionHandler(success: true)
            },
            failure: { error in
                print(error.localizedDescription)
                completionHandler(success: false)
            }
        )
    }
    
    
}