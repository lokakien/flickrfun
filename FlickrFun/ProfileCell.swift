//
//  ProfileCell.swift
//  FlickrFun
//
//  Created by Loka on 06/05/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit
import SDWebImage
import MBProgressHUD



class ProfileCell: UICollectionViewCell {
    
    @IBOutlet weak var aboutLabel: UILabel!
    @IBOutlet weak var postLabel: UILabel!
    @IBOutlet weak var followerLabel: UILabel!
    @IBOutlet weak var followingLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var followingButton: UIButton!
    
    var delegate: UIViewController!

    
    var people: People!{
        didSet{
            setTapGesture()
            profileImageView.layer.cornerRadius = profileImageView.frame.width/2
            profileImageView.sd_setImageWithURL(people.iconURL, placeholderImage: UIImage.init(named: "Placeholder"))
            updateDisplay()
        }
    }
    
    func updateDisplay(){
//        guard people.following != nil && people.followers != nil && people.aboutPeople != nil  else{
//            return
//        }
        if var aboutText = people.aboutPeople{
            if aboutText.characters.count > 100{
                aboutText = "\((aboutText as NSString).substringToIndex(100))... More"
            }
            aboutLabel.text = aboutText
            postLabel.text = "\(people.totalPhotos!)\n photos"
        }
        
        if let following = people.following {
            followingLabel.text = "\(following.total)\nfollowing"
        }
        
        if let followers = people.followers{
            followerLabel.text = "\(followers.total)\n followers"
        }
        updateFollowButton()
    }

    func setTapGesture(){
        let followerTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(showFollow))
        followerLabel.addGestureRecognizer(followerTapGesture)
        let followingTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(showFollow))
        followingLabel.addGestureRecognizer(followingTapGesture)
    }
    
    
    func showFollow(sender: UITapGestureRecognizer){
        guard people.followers != nil && people.following != nil else{
            return
        }
        
        let followVC = delegate.storyboard!.instantiateViewControllerWithIdentifier("followViewController") as! FollowViewController
         if sender.view! == followerLabel{
            followVC.followers = people.followers!
         }else{
            followVC.followers = people.following!
        }
        delegate.navigationController?.showViewController(followVC, sender: delegate)
        
    }
    
    func checkIfFollowing()->Bool{
        let filterArray = User.sharedInstance().following!.peoples.filter{$0.userID == people.userID}
        if !filterArray.isEmpty{
            return true
        }else{
            return false
        }
    }
    
    func updateFollowButton(){
        guard followingButton != nil else{
            return
        }
        let isFollowing = checkIfFollowing()
        if isFollowing{
            followingButton.setTitle("Following", forState: .Normal)
        }else{
            followingButton.setTitle("Follow+", forState: .Normal)
        }
    }
    
    @IBAction func addRemoveFollowing(sender: AnyObject) {
        let isFollowing = checkIfFollowing()
        MBProgressHUD.showHUDAddedTo(delegate.view, animated: true)
        if isFollowing{
            Flickr.sharedInstance().removeFollowing(people, completionHandler: { (success) in
                if success{
                    User.sharedInstance().following!.removePeople(self.people)
                    self.updateFollowButton()
                }
                MBProgressHUD.hideHUDForView(self.delegate.view, animated: true)
            })
        }else{
            Flickr.sharedInstance().addFollowing(people, relationship: People.relationType.publicPerson , completionHandler: { (success) in
                if success{
                    User.sharedInstance().following!.addPeople(self.people)
                    self.updateFollowButton()
                    
                }
                MBProgressHUD.hideHUDForView(self.delegate.view, animated: true)
            })
        }
    }
    
    
}
