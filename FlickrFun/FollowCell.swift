//
//  FollowCell.swift
//  FlickrFun
//
//  Created by Loka on 06/05/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit
import SDWebImage
import MBProgressHUD

@objc protocol FollowCellDelegate{
    func followCell(followCell: FollowCell, didTapOnFollowButton detected: Bool)
    optional func followCell(followCell: FollowCell, didTapOnIconImageView detected: Bool)
}

class FollowCell: UITableViewCell {
    @IBOutlet weak var peopleIConImageView: UIImageView!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    var delegate: FollowCellDelegate?
    var iconImageTapGesture: UITapGestureRecognizer!
    
    var people: People!{
        didSet{
            peopleIConImageView.layer.cornerRadius = peopleIConImageView.frame.width/2
            peopleIConImageView.sd_setImageWithURL(people.iconURL, placeholderImage: UIImage.init(named: "Placeholder"))
            peopleIConImageView.userInteractionEnabled = true
            if people.fullname != nil && people.fullname != "" {
                nameLabel.text = people.fullname
            }else{
                nameLabel.text = people.username
            }
            updateFollowButton()
            let recognizers = peopleIConImageView.gestureRecognizers
            if recognizers != nil {
                for recognizer in recognizers!{
                    peopleIConImageView.removeGestureRecognizer(recognizer)
                }
            }
            iconImageTapGesture = UITapGestureRecognizer(target: self, action: #selector(iconImageTapped))
            peopleIConImageView.addGestureRecognizer(iconImageTapGesture)
        }
    }
    
    @IBAction func followButtonPressed(sender: AnyObject) {
        if let delegate = self.delegate{
            delegate.followCell(self, didTapOnFollowButton: true)
        }
    }
    
    func checkIfFollowing(people: People)->Bool{
        let filterArray = User.sharedInstance().following!.peoples.filter{$0.userID == people.userID}
        if !filterArray.isEmpty{
            return true
        }else{
            return false
        }
    }
    
    func updateFollowButton(){
        let isFollowing = checkIfFollowing(people)
        if isFollowing{
            followButton.setTitle("Following", forState: .Normal)
        }else{
            followButton.setTitle("Follow+", forState: .Normal)
        }
    }
    
    func iconImageTapped(){
        if let delegate = self.delegate{
           delegate.followCell!(self, didTapOnIconImageView: true)
        }
    }
    
    
}
