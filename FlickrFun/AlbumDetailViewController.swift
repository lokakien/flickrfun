//
//  AlbumCollectionViewController.swift
//  FlickrFun
//
//  Created by Loka on 03/05/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

class AlbumDetailViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var album: Album!
    var people: People!
//    var totalPages: Int = 2
//    var currentPage: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.hidden = false
        collectionView.dataSource = self
        collectionView.delegate = self
        refreshData()
    }
    
    override func viewWillAppear(animated: Bool) {
        collectionView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    func refreshData(){
        if album.id == "faves"{
            Flickr.sharedInstance().getFavesAlbumPhotos(people, currentPage: album.currentPage, completionHandler: { (photos, currentPage, totalPages) in
                //pass data here
                self.album.photos = photos
                self.album.totalPages = totalPages
                self.album.currentPage = currentPage
                self.collectionView.reloadData()
            })
        } else{
            Flickr.sharedInstance().getAlbumPhotos(album, people: people, currentPage: album.currentPage) { (photos, currentPage, totalPages) in
                self.album.photos = photos
                self.album.currentPage = currentPage
                self.album.totalPages = totalPages
                self.collectionView.reloadData()
            }
        }
    }
}

extension AlbumDetailViewController: UICollectionViewDataSource{
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return album.photos.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("photoCell", forIndexPath: indexPath) as! PhotoCell
        cell.photo = album.photos[indexPath.item]
        return cell
    }
}

extension AlbumDetailViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        var numberOfRows: Int = 0
        if collectionView.frame.width < collectionView.frame.height {
            numberOfRows = 2
        }else {
            numberOfRows = 3
        }
        let cellWidth = collectionView.frame.width / CGFloat(numberOfRows)
        let size = CGSizeMake(cellWidth, cellWidth)
        return size
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.0
    }
    
}


extension AlbumDetailViewController: UICollectionViewDelegate{
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("segueAlbumDetailVCtoPhotoVC", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueAlbumDetailVCtoPhotoVC" {
            let photoVC = segue.destinationViewController as! PhotoViewController
            photoVC.photos = album.photos
            let indexPath = collectionView.indexPathsForSelectedItems()!.first!
            photoVC.selected = indexPath.item
        }
    }
}







