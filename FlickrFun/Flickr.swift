//
//  Flickr.swift
//  FlickrFun
//
//  Created by Loka on 06/05/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import  UIKit

class Flickr{
    class func sharedInstance()-> Flickr{
        struct Singleton{
            static let sharedInstance = Flickr()
        }
        return Singleton.sharedInstance
    }
    
    func getPeopleInfo(people: People, completionHandler: (success: Bool)->()){
        let extraParameters = [Constants.FlickrParameter.UserID: people.userID!]
        FlickrClient.sharedInstance().FlickrGetMethod(Constants.FlickrMethod.GetPeopleInfo, extraParameter: extraParameters, successCompletionHandler: { (parsedJSON) in
            self.getParsedJSONToPeopleInfo(parsedJSON, people: people)
            completionHandler(success: true)
            }, failCompletionHandler: {
                print ("failed to get people info")
                completionHandler(success: false)
        })
    }
    
    func getUserPublicFollowing(people: People, completionHandler: (following:Followers)->()){
        let extraParameters = [Constants.FlickrParameter.UserID: people.userID!]
        FlickrClient.sharedInstance().FlickrGetMethod(Constants.FlickrMethod.GetUserPublicFollowing, extraParameter: extraParameters, successCompletionHandler: { (parsedJSON) in
            //            print (parsedJSON)
            let following = self.getParsedJSONToFollowers(parsedJSON)
            completionHandler(following: following)
            }, failCompletionHandler: {
                print ("failed")
        })
    }
    
    
    func getUserFollowing(people: People, completionHandler: (following:Followers)->()){
        let extraParameters = [Constants.FlickrParameter.UserID: people.userID!]
        FlickrClient.sharedInstance().FlickrGetMethod(Constants.FlickrMethod.GetUserFollowing, extraParameter: extraParameters, successCompletionHandler: { (parsedJSON) in
            //            print (parsedJSON)
            let following = self.getParsedJSONToFollowers(parsedJSON)
            completionHandler(following: following)
            }, failCompletionHandler: {
                print ("failed")
        })
    }
    
    func getFollower(people: People, completionHandler: (followers:Followers)->()){
        let extraParameters = [Constants.FlickrParameter.UserID: people.userID!]
        FlickrClient.sharedInstance().FlickrGetMethod(Constants.FlickrMethod.GetFollowers, extraParameter: extraParameters, successCompletionHandler: { (parsedJSON) in
            let followers = self.getParsedJSONToFollowers(parsedJSON)
            completionHandler(followers: followers)
            }, failCompletionHandler: {
                print ("failed")
        })
    }
    
    func addFollowing(people: People, relationship: People.relationType, completionHandler: (success: Bool)->()){
        var isFamily = "0"
        var isFriend = "0"
        switch relationship {
        case .family:
            isFamily = "1"
            break
        case .friend:
            isFriend = "1"
            break
        default:
            break
        }
        let extraParameters = [Constants.FlickrParameter.UserID: people.userID!,
                               Constants.FlickrParameter.Friend: isFriend,
                               Constants.FlickrParameter.Family: isFamily]
        FlickrClient.sharedInstance().FlickrPostMethod("flickr.contacts.add", extraParameter: extraParameters, successCompletionHandler: { (parsedJSON) in
            if parsedJSON["stat"] as! String == "ok"{
                completionHandler(success: true)
            }else{
                completionHandler(success: false)
            }
            }, failureHandler: {
                print ("failed")
                completionHandler(success: false)
        })
    }
    
    func removeFollowing(people: People, completionHandler: (success: Bool)->()){
        let extraParameters = [Constants.FlickrParameter.UserID: people.userID!]
        FlickrClient.sharedInstance().FlickrPostMethod("flickr.contacts.remove", extraParameter: extraParameters, successCompletionHandler: { (parsedJSON) in
            if parsedJSON["stat"] as! String == "ok"{
                completionHandler(success: true)
            }else{
                completionHandler(success: false)
            }
            
            }, failureHandler: {
                completionHandler(success: false)
        })
    }
    
    func getPhotosInfo(people: People, page: Int, totalPages: Int, completionHandler: (photos: [Photo], currentPage: Int, totalPages: Int)->()) {
        let extraParameter = [Constants.FlickrParameter.UserID: people.userID!,
                              Constants.FlickrParameter.Page: String(page),
                              Constants.FlickrParameter.Sort: "date-taken-desc"]
        
        let flickrClient = FlickrClient.sharedInstance()
        flickrClient.FlickrGetMethod(Constants.FlickrMethod.GetPhoto, extraParameter: extraParameter,successCompletionHandler:{
            (parsedJSON) in
            let (photos, currentPage, totalPages) = self.getParsedJSONToPhotoArray(parsedJSON)
            completionHandler(photos: photos, currentPage: currentPage, totalPages: totalPages)
            }, failCompletionHandler: {
                print("fail to get photos")
                completionHandler(photos: [Photo](),currentPage: page - 1,totalPages: totalPages)
        })
    }
    
    func addRemoveFavourite(add: Bool, photoID: String, completionHandler: (success:Bool, message: String?)->()){
        let extraParameter = [Constants.FlickrParameter.PhotoID : photoID]
        let method: String!
        if add{
            method = Constants.FlickrMethod.AddFavourites
        }else {
            method = Constants.FlickrMethod.RemoveFavourites
        }
        
        FlickrClient.sharedInstance().FlickrPostMethod(method, extraParameter: extraParameter, successCompletionHandler: { (parsedJSON) in
            let stat = parsedJSON["stat"] as? String
            let message = parsedJSON["message"] as? String
            if stat != nil && stat == "ok"{
                completionHandler(success: true, message: message)
            }else{
                completionHandler(success: false, message: message)
            }
            }, failureHandler: {
                print ("cant add favourite")
        })
    }
    
    func getComments(photoID: String , completionHandler: (comments: [Comment])->()){
        let extraParameter = [Constants.FlickrParameter.PhotoID: photoID]
        
        FlickrClient.sharedInstance().FlickrGetMethod(Constants.FlickrMethod.GetComments, extraParameter: extraParameter, successCompletionHandler: { (parsedJSON) in
            let comments = self.getParsedJSONToCommentArray(parsedJSON)
            completionHandler(comments: comments)
            },failCompletionHandler:  {
                print ("failed to get comments")
        })
    }
    
    func addComment(photoID: String, commentText: String, completionHandler: (comment:Comment?)->()){
        let extraParameter = [ Constants.FlickrParameter.CommentText: commentText,
                               Constants.FlickrParameter.PhotoID : photoID]
        FlickrClient.sharedInstance().FlickrPostMethod(Constants.FlickrMethod.AddComment, extraParameter: extraParameter, successCompletionHandler: {
            (parsedJSON) in
            let comment = self.getParsedJSONtoComment(parsedJSON)
            completionHandler(comment: comment)
            }, failureHandler: {
                print ("fail to add comment")
        })
    }
    
    func getInterestingPhotosOfDay(page: Int, totalPages: Int, completionHandler: (photos:[Photo], currentPage: Int, totalPages: Int)-> ()){
        let extraParameter = [Constants.FlickrParameter.Page: String(page)]
        
        FlickrClient.sharedInstance().FlickrGetMethod(Constants.FlickrMethod.GetInterestingPhotoList, extraParameter: extraParameter, successCompletionHandler: { (parsedJSON) in
            let (photos, currentPage, totalPages) = self.getParsedJSONToPhotoArray(parsedJSON)
            completionHandler(photos: photos, currentPage: currentPage, totalPages: totalPages)
            }, failCompletionHandler: {
                print("fail to get interesting photo list")
                completionHandler(photos: [Photo](),currentPage: page - 1, totalPages: totalPages)
        })
    }
    
    func searchPhoto(searchText: String, page: Int, totalPages: Int, completionHandler: (photos:[Photo], currentPage: Int, totalPages: Int)->()){
        let extraParameter = [Constants.FlickrParameter.Text: searchText,
                              Constants.FlickrParameter.Page: String(page),
                              Constants.FlickrParameter.Sort: Constants.FlickrParameterValue.Sort]
        
        FlickrClient.sharedInstance().FlickrGetMethod(Constants.FlickrMethod.SearchPhoto, extraParameter: extraParameter, successCompletionHandler: { (parsedJSON) in
            let (photos, currentPage, totalPages) = self.getParsedJSONToPhotoArray(parsedJSON)
            completionHandler(photos: photos, currentPage: currentPage, totalPages: totalPages)
        }) {
            print("failed to search photo")
            completionHandler(photos: [Photo](),currentPage: page - 1,totalPages: totalPages)
        }
    }
    
    //get who faves a photo
    func getFavourites(photoID: String, page: Int, completionHandler: (peoples: [People],currentPage: Int, totalPages: Int)->()){
        let extraParameter = [Constants.FlickrParameter.PhotoID: photoID,
                              Constants.FlickrParameter.Page: String(page),
                              Constants.FlickrParameter.PerPage: "50"]
        FlickrClient.sharedInstance().FlickrGetMethod(Constants.FlickrMethod.GetFavourites, extraParameter: extraParameter, successCompletionHandler: { (parsedJSON) in
            let (peoples, currentPage, totalPages) = self.getParsedJSONToFavouriteArray(parsedJSON)
            completionHandler(peoples: peoples,currentPage: currentPage, totalPages: totalPages)
            
            }, failCompletionHandler: {
                print ("failed to get favourites")
        })
        
    }
    
    func getFavesAlbum(pageNumber: Int, completionHander:(Album?)->()){
        let extraParameter = ["page": String(pageNumber)]
        FlickrClient.sharedInstance().FlickrGetMethod(Constants.FlickrMethod.GetFavouriteAlbum, extraParameter: extraParameter, successCompletionHandler: { (parsedJSON) in
            let album: Album? = self.getParsedJSONToFavAlbum(parsedJSON)
            completionHander(album)
            }, failCompletionHandler: {
                print ("cant get faves album")
        })
    }
    
    func getAlbums(people: People, currentPage: Int, totalPages: Int, completionHandler:(albums:[Album], currentPage: Int, totalPages: Int)->()){
        let extraParameters : [String: String] = [Constants.FlickrParameter.UserID: people.userID,
                                                  Constants.FlickrParameter.Page: String(currentPage)]
        FlickrClient.sharedInstance().FlickrGetMethod(Constants.FlickrMethod.GetAlbums, extraParameter: extraParameters, successCompletionHandler: { (parsedJSON) in
            let (albums, currentPage, totalPages) = self.getParsedJSONtoAlbums(parsedJSON)
            completionHandler(albums: albums, currentPage: currentPage, totalPages: totalPages)
            }, failCompletionHandler: {
                print ("cant get albums")
                completionHandler(albums: [Album](), currentPage: currentPage - 1, totalPages: totalPages)
        })
    }
    
    
    func getFavesAlbumPhotos(people: People, currentPage: Int, completionHandler: ([Photo], currentPage: Int, totalPages: Int)->()){
        let extraParameters = [Constants.FlickrParameter.UserID: people.userID!,
                               Constants.FlickrParameter.Page: String(currentPage)]
        FlickrClient.sharedInstance().FlickrGetMethod(Constants.FlickrMethod.GetFavouriteAlbum, extraParameter: extraParameters, successCompletionHandler: { (parsedJSON) in
            let (photos, currentPage, totalPages) = self.getParsedJSONToPhotoArray(parsedJSON)
            completionHandler(photos, currentPage: currentPage, totalPages: totalPages)
            }, failCompletionHandler: {
                
        })
        
    }
    
    func getAlbumPhotos(album: Album, people: People, currentPage: Int, completionHandler: ([Photo], currentPage: Int, totalPages: Int)->()){
        let extraParameters = [Constants.FlickrParameter.AlbumID: album.id!,
                               Constants.FlickrParameter.UserID: people.userID!,
                               Constants.FlickrParameter.Page: String(currentPage)]
        
        FlickrClient.sharedInstance().FlickrGetMethod(Constants.FlickrMethod.GetAlbumPhotos, extraParameter: extraParameters, successCompletionHandler: { (parsedJSON) in
            let (photos,currentPage, totalPages) = self.getParsedJSONfromPhotosetToPhotoArray(parsedJSON, owner: people)
            completionHandler(photos,currentPage: currentPage, totalPages: totalPages)
            }, failCompletionHandler: {
                print ("cant get album photos")
        })
    }
    
    
    //for user
    func getGroups(people: People, completionHandler: [Group]-> ()){
        let extraParameters = [Constants.FlickrParameter.UserID: people.userID!]
        
        FlickrClient.sharedInstance().FlickrGetMethod(Constants.FlickrMethod.GetGroups, extraParameter: extraParameters, successCompletionHandler: { (parsedJSON) in
            let groups = self.getParsedJSONToGroup(parsedJSON)
            completionHandler(groups)
            }, failCompletionHandler: {
                print ("cant get groups!")
                completionHandler([Group]())
        })
    }
    
    //for other people. same method but return value is different. no is_member in here
    func getGroupsForPeople(people: People, completionHandler: [Group]-> ()){
        let extraParameters = [Constants.FlickrParameter.UserID: people.userID!]
        
        FlickrClient.sharedInstance().FlickrGetMethod(Constants.FlickrMethod.GetGroups, extraParameter: extraParameters, successCompletionHandler: { (parsedJSON) in
            let groups = self.getParsedJSONToGroupForUser(parsedJSON, people: people)
            completionHandler(groups)
            }, failCompletionHandler: {
                print ("cant get groups!")
                completionHandler([Group]())
        })
    }
    
    
    
    
    
    
    func setPhotoPermission(photos: [Photo], permission: Photo.permissionType, completionHandler: (success:Bool)->()){
        var isFamily = "0", isFriend = "0", isPublic = "0"
        switch permission{
        case .isPublic:
            isPublic = "1"
            isFriend = "1"
            isFamily = "1"
            break
        case .isFriend:
            isFriend = "1"
            isFamily = "1"
            break
        case .isFamily:
            isFamily = "1"
            break
        case .isPrivate:
            break
        default:
            print ("undefined")
        }
        
        var resultArray:[String] = []
        
        for i in 0...photos.count - 1{
            let extraParameters: [String: String] = [Constants.FlickrParameter.PhotoID: photos[i].id,
                                                     Constants.FlickrParameter.IsFamily: isFamily,
                                                     Constants.FlickrParameter.IsFriend: isFriend,
                                                     Constants.FlickrParameter.IsPublic: isPublic
            ]
            FlickrClient.sharedInstance().FlickrPostMethod(Constants.FlickrMethod.SetPhotoPermission, extraParameter: extraParameters, successCompletionHandler: { (parsedJSON) in
                let stat: String = parsedJSON["stat"] as! String
                if stat == "ok"{
                    photos[i].permission = permission
                }
                resultArray.append(stat)
                if resultArray.count == photos.count{
                    let successResult: [String] = resultArray.filter{$0 == "ok"}
                    if successResult.count == photos.count{
                        completionHandler(success: true)
                    }else{
                        completionHandler(success: false)
                    }
                }
                
                }, failureHandler: {
                    print ("cant set permission")
                    resultArray.append("fail'")
                    if resultArray.count == photos.count{
                        completionHandler(success: false)
                    }
            })
            
        }
        
    }
    
    func deletePhoto(photos: [Photo], people: People, completionHandler: (success: Bool)->()){
        var resultArray: [String] = []
        for i in 0...photos.count - 1{
            let extraParameter = [Constants.FlickrParameter.PhotoID: photos[i].id!]
            FlickrClient.sharedInstance().FlickrPostMethod(Constants.FlickrMethod.DeletePhoto, extraParameter: extraParameter, successCompletionHandler: { (parsedJSON) in
                print (parsedJSON)
                let stat = parsedJSON["stat"] as! String
                if stat == "ok"{
                    for j in 0...people.photos.count - 1{
                        if people.photos[j].id == photos[i].id{
                            people.photos.removeAtIndex(j)
                            break
                        }
                    }
                }
                resultArray.append(stat)
                if resultArray.count == photos.count{
                    let successResult = resultArray.filter{$0 == "ok"}
                    if successResult.count == photos.count{
                        completionHandler(success: true)
                    }else{
                        completionHandler(success: false)
                    }
                }
                
                }, failureHandler: { 
                    print ("failed to delete photo")
                    completionHandler(success: false)
            })
        }
        
    }
    
    func createAlbum(newAlbumTitle: String, primaryPhoto: Photo, completionHandler: (Album?)->()){
        let extraParameter: [String: String] = [Constants.FlickrParameter.Title: newAlbumTitle,
                                               Constants.FlickrParameter.PrimaryPhotoID: primaryPhoto.id]
        
        FlickrClient.sharedInstance().FlickrPostMethod(Constants.FlickrMethod.CreateAlbum, extraParameter: extraParameter, successCompletionHandler: { (parsedJSON) in
            guard let stat = parsedJSON["stat"] as? String else {
                completionHandler(nil)
                return
            }
            if stat == "ok"{
                let photosetJSON = parsedJSON["photoset"] as! [String: AnyObject]
                let newAlbum = Album()
                newAlbum.id = photosetJSON["id"] as! String
                newAlbum.title = newAlbumTitle
                newAlbum.primaryFarmID = Int(primaryPhoto.farmID)
                newAlbum.primaryServerID = primaryPhoto.serverID
                newAlbum.primaryID = primaryPhoto.id
                newAlbum.primarySecret = primaryPhoto.secret
                completionHandler(newAlbum)
            }else{
                completionHandler(nil)
            }
            }, failureHandler: {
                print ("failed to create album")
                completionHandler(nil)
        })
    }
    
    func addPhotoToAlbum(photos: [Photo],album: Album, completionHandler: ((success: Bool, successAddedPhotos: [Photo])->())){
        var successAddedPhoto: [Photo] = []
        var results: [String] = []
        for photo in photos{
            let extraParameter: [String: String] = [Constants.FlickrParameter.AlbumID: album.id,
                                  Constants.FlickrParameter.PhotoID: photo.id]
            FlickrClient.sharedInstance().FlickrPostMethod(Constants.FlickrMethod.AddPhotoToAlbum, extraParameter: extraParameter, successCompletionHandler: { (parsedJSON) in
                let stat = parsedJSON["stat"] as! String
                results.append(stat)
                if stat == "ok"{
                    successAddedPhoto.append(photo)
                }
                if results.count == photos.count{
                    let failedResults = results.filter{$0 == "failed"}
                    if failedResults.isEmpty{
                        completionHandler(success: true, successAddedPhotos: successAddedPhoto)
                    }else{
                        completionHandler(success: false, successAddedPhotos: successAddedPhoto)
                    }
                }
                }, failureHandler: {
                    results.append("failed")
                    if results.count == photos.count{
                        completionHandler(success: false, successAddedPhotos: successAddedPhoto)
                    }
            })
            
        }
    }
    
}
