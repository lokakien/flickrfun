//
//  GroupInfo.swift
//  FlickrFun
//
//  Created by Loka on 04/05/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

class GroupInfo{
    var description: String!
    var rules: String!
    var privacy: Int! //3 = public
    var topicCount: Int!
    var roles: GroupRoles!
//    var blast: String!
    
}

enum GroupRoles{
    case isMember
    case isModerator
    case isAdmin
}

class GroupRestrictions{
    var photoOK: Bool!
    var videoOK: Bool!
    var imageOK: Bool!
    var screenOK: Bool!
    var artOK: Bool!
    var safeOK: Bool!
    var moderateOK: Bool!
    var restrictedOK: Bool!
}