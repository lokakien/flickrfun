//
//  Album.swift
//  FlickrFun
//
//  Created by Loka on 02/05/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

class Album {
    var id: String!
    var title: String!
    var numberOfPhotos = 0
    var numberOfVideos = 0
    var photos: [Photo] = []
    var albumCoverUrl: NSURL{
        get{
            return NSURL.init(string: "https://farm\(primaryFarmID).staticflickr.com/\(primaryServerID)/\(primaryID)_\(primarySecret)_q.jpg")!
            
        }
    }
    var primaryFarmID: Int!
    var primaryServerID: String!
    var primaryID: String!
    var primarySecret: String!
    var currentPage: Int!
    var totalPages: Int!
    
    init(){
        currentPage = 1
        totalPages = 2
        photos = [Photo]()
    }
}
