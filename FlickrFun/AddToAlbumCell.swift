//
//  AddToAlbumCell.swift
//  FlickrFun
//
//  Created by Loka on 12/05/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit
import SDWebImage


class AddToAlbumCell: UITableViewCell {
    var album: Album!{
        didSet{
            albumImageView.sd_setImageWithURL(album.albumCoverUrl, placeholderImage: UIImage(named: "Placeholder"))
            albumTitleLabel.text = album.title
            albumDetailLabel.text = "\(album.numberOfPhotos) photos and videos"
            
        }
    }

    @IBOutlet weak var albumDetailLabel: UILabel!
    @IBOutlet weak var albumTitleLabel: UILabel!
    @IBOutlet weak var albumImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }


}
