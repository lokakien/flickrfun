//
//  Group.swift
//  FlickrFun
//
//  Created by Loka on 03/05/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

class Group{
    var id: String!{
        didSet{
            coverPhotoURL = NSURL.init(string: "https://flickr.com/buddyicons/\(id).jpg")
        }
    }
    var title: String!
    var numberOfGroupMembers: Int!
    var numberOfPhotos: Int!
    var coverPhotoURL: NSURL!
    var isMember: Bool!
    var groupInfo: GroupInfo?
    var invitationOnly: Bool!
    
    init(){
        invitationOnly = false
        isMember = false
    }
}
