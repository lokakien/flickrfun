//
//  AddToAlbumViewController.swift
//  FlickrFun
//
//  Created by Loka on 12/05/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit
import MBProgressHUD

class AddToAlbumViewController: UIViewController {
    var people: People!
    var selectedPhotos: [Photo]!
    var alertController: UIAlertController!
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        // Do any additional setup after loading the view.
        if people.albums.isEmpty{
            Flickr.sharedInstance().getAlbums(people, currentPage: 1, totalPages: people.albumsTotalPages, completionHandler: { (albums, currentPage, totalPages) in
                self.people.albums = albums
                self.people.albumsCurrentPage = currentPage
                self.people.albumsTotalPages = totalPages
                self.tableView.reloadData()
            })
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func dismissVC(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func addNewAlbum(sender: AnyObject) {
        alertController = UIAlertController(title: "Create New Album", message: nil, preferredStyle: .Alert)
        alertController.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "Enter New Album Name"
            textField.addTarget(self, action: #selector(self.alertTextFieldDidChange), forControlEvents: .EditingChanged)
        }
        let addAction = UIAlertAction(title: "OK", style: .Default, handler: confirmAddNewAlbum )
        addAction.enabled = false
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        alertController.addAction(addAction)
        alertController.addAction(cancelAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func confirmAddNewAlbum(alertAction: UIAlertAction){
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        let newAlbumTitle = alertController.textFields!.first!.text!
        Flickr.sharedInstance().createAlbum(newAlbumTitle, primaryPhoto: selectedPhotos.first!, completionHandler: {
            album in
            MBProgressHUD.hideHUDForView(self.view, animated: true)
            if let album = album{
                self.people.albums.insert(album, atIndex: 0)
//                self.people.albums.append(album)
                album.numberOfPhotos = album.numberOfPhotos + 1
                album.photos.append(self.selectedPhotos.first!)
                self.tableView.reloadData()
            }else{
                let alertController = UIAlertController(title: "Failed to create new album", message: nil, preferredStyle: .Alert)
                let okAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
                alertController.addAction(okAction)
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        })
    }
    
    func addPhotoToAlbum(album: Album){
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        Flickr.sharedInstance().addPhotoToAlbum(self.selectedPhotos, album: album, completionHandler: { (success, successAddedPhotos) in
            MBProgressHUD.hideHUDForView(self.view, animated: true)
            for photo in successAddedPhotos{
                album.photos.append(photo)
                album.numberOfPhotos = album.numberOfPhotos + 1
            }
            if success{
                self.dismissViewControllerAnimated(true, completion: nil)
            }else{
                let failAlertController = UIAlertController(title: "Failed to added photos", message: "", preferredStyle: .Alert)
                let okAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
                failAlertController.addAction(okAction)
                self.presentViewController(failAlertController, animated: true, completion: nil)
            }
            
        })

    }
    
    func alertTextFieldDidChange(){
        let textField = alertController.textFields!.first!
        let addAction = alertController.actions.first!
        if textField.text?.characters.count > 0{
            addAction.enabled = true
        }else{
            addAction.enabled = false
        }
    }
    
    
}

extension AddToAlbumViewController: UITableViewDataSource{
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return people.albums.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("addToAlbumCell", forIndexPath: indexPath) as! AddToAlbumCell
        cell.album = people.albums[indexPath.row]
        return cell
    }
}

extension AddToAlbumViewController: UITableViewDelegate{
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let album = people.albums[indexPath.row]
        self.addPhotoToAlbum(album)
    }
}



