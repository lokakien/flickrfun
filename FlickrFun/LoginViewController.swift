//
//  ViewController.swift
//  FlickrOffline
//
//  Created by Loka on 21/04/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit
import OAuthSwift

class LoginViewController: UIViewController {
    
    @IBOutlet weak var loginButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUIEnabled(enabled: Bool){
        loginButton.enabled = enabled
        if (enabled){
            loginButton.alpha = 1.0
            return
        }
        loginButton.alpha = 0.5
    }
    
    @IBAction func login(sender: AnyObject) {
        setUIEnabled(false)
        FlickrClient.sharedInstance().doOAuthFlickr{
            success
            in
            if (success){
                let tabbarcontroller = self.storyboard!.instantiateViewControllerWithIdentifier("mainTabBarController") as! UITabBarController
                tabbarcontroller.selectedIndex = 2
                
                self.presentViewController(tabbarcontroller, animated: true, completion: nil)
                
                return
            }
            self.setUIEnabled(true)
        }
    }
    

    
}

