//
//  GroupViewController.swift
//  FlickrFun
//
//  Created by Loka on 03/05/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

class GroupViewController: UIViewController {
    var people: People!
//    var groups: [Group]!
    var swipeDownGesture: UISwipeGestureRecognizer!
    var refreshControl: UIRefreshControl?

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        people = User.sharedInstance()
        tableView.dataSource = self
        tableView.delegate = self
        refreshData()
        tableView.alwaysBounceVertical = true
        refreshControl = pullDownRefresh(self, selector: #selector(refreshData))
        tableView.addSubview(refreshControl!)
    }
    
    override func viewWillAppear(animated: Bool) {
        swipeDownGesture = UISwipeGestureRecognizer.init(target: self, action: #selector(swipeDownDetected))
        swipeDownGesture.direction = .Down
        self.view.addGestureRecognizer(swipeDownGesture)
        if let indexPath = tableView.indexPathForSelectedRow{
             tableView.deselectRowAtIndexPath(indexPath, animated: true)
        }
        tableView.reloadData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.view.removeGestureRecognizer(swipeDownGesture)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refreshData(){
        Flickr.sharedInstance().getGroups(people) { groups in
            //pass groups
            self.refreshControl?.endRefreshing()
            self.people.groups = groups
            self.tableView.reloadData()
        }
    }
    
    func swipeDownDetected(){
        refreshData()
    }

}

extension GroupViewController: UITableViewDataSource{
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = people.groups.count
        if count == 0{
            tableView.backgroundView = getBackgroundLabel(tableView)
        }else {
            tableView.backgroundView = nil
        }
        return count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("groupCell") as! GroupCell
        cell.group = self.people.groups[indexPath.row]
        return cell
    }
}

extension GroupViewController: UITableViewDelegate{
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let frame = self.view.frame
        return frame.width/3
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("segueGroupVCToGroupDetailVC", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueGroupVCToGroupDetailVC"{
            let navigationController = segue.destinationViewController as! UINavigationController
            let groupDetailVC = navigationController.viewControllers.first! as! GroupDetailViewController
            let indexPath = tableView.indexPathForSelectedRow
            groupDetailVC.group = self.people.groups[indexPath!.row]
        }
    }
}
