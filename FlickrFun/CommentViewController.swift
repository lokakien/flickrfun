//
//  CommentViewController.swift
//  FlickrFun
//
//  Created by Loka on 28/04/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit



class CommentViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var postButton: UIButton!
    @IBOutlet weak var commentTextView: UITextView!
    var photo: Photo!
    
    var tapGesture: UITapGestureRecognizer?
    var favesTapGesture: UITapGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if photo.comments == nil {
            photo.comments = [Comment]()
        }
        setPlaceholderForTextView(commentTextView)
        postButton.enabled = false
        let navigationbar = self.navigationController!.navigationBar
        let label = UILabel.init(frame: CGRectMake(navigationbar.frame.origin.x, navigationbar.frame.origin.y, navigationbar.frame.width, navigationbar.frame.height))
        label.textAlignment = NSTextAlignment.Right
        label.text = "\(photo.numberOfFaves) Faves"
        self.navigationItem.titleView = label
        commentTextView.delegate = self
        tableView.dataSource = self
//        tableView.delegate = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 70
        commentTextView.layer.borderColor = UIColor.grayColor().CGColor
        commentTextView.layer.borderWidth = 1.0
        Flickr.sharedInstance().getComments(photo.id) { (comments) in
            //code to pass comments
            if comments.count > 0{
                self.photo.comments = comments
                self.tableView.reloadData()
                let indexPath = NSIndexPath.init(forRow: comments.count - 1, inSection: 0)
                self.tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Bottom, animated: false)
            }
        }
        commentTextView.textContainerInset = UIEdgeInsetsMake(4, 4, 4, postButton.frame.width)
    }
    
    override func viewWillAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.keyboardWillShow), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.keyboardWillHide), name: UIKeyboardWillHideNotification, object: nil)

    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    @IBAction func dismissVC(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func keyboardWillShow(notification: NSNotification){
         tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapDetected))
        self.view.addGestureRecognizer(tapGesture!)
        if let userInfo = notification.userInfo{
            let keyboardFrame = userInfo[UIKeyboardFrameBeginUserInfoKey]?.CGRectValue()
            self.view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y - keyboardFrame!.height, self.view.frame.width, self.view.frame.height)
        }
    }
    
    func keyboardWillHide(notification: NSNotification){
        if let tapGesture = tapGesture {
            self.view.removeGestureRecognizer(tapGesture)
        }

        if notification.userInfo != nil{
//            let keyboardFrame = userInfo[UIKeyboardFrameBeginUserInfoKey]?.CGRectValue()
            self.view.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
        }
    }
    
    func tapDetected(){
        commentTextView.resignFirstResponder()
    }

    @IBAction func nextButtonPressed(sender: UIBarButtonItem) {
        self.performSegueWithIdentifier("segueCommentVCToFavesVC", sender: self)
    }
    
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueCommentVCToFavesVC" {
            let favouriteVC = segue.destinationViewController as! FavouriteViewController
            favouriteVC.photo = photo
       }
        
        if segue.identifier == "segueCommentVCToPeopleVC"{
            let navigationController = segue.destinationViewController as! UINavigationController
            let peopleVC = navigationController.viewControllers.first as! PeopleViewController
            let indexPath = tableView.indexPathForCell(sender as! CommentCell)!
//            let indexPath = tableView.indexPathForSelectedRow!
            peopleVC.people = self.photo.comments[indexPath.row].author
        }
    }
    
    @IBAction func postComment(sender: AnyObject) {
        Flickr.sharedInstance().addComment(photo.id, commentText: commentTextView.text) { (comment) in
            if let comment = comment {
                self.photo.addComment(comment)
                self.tableView.reloadData()
                let indexPath = NSIndexPath.init(forRow: self.photo.comments.count - 1, inSection: 0)
                self.tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Bottom, animated: true)
            }else {
                let alertController = UIAlertController.init(title: "Unable to post comment", message: "Please try again later", preferredStyle: .Alert)
                let action = UIAlertAction.init(title: "OK", style: .Default, handler: nil)
                alertController.addAction(action)
                self.presentViewController(alertController, animated: true, completion: nil)
            }
            //success handler here
        }
        commentTextView.text = ""
        setPlaceholderForTextView(commentTextView)
        commentTextView.resignFirstResponder()
    }

}

extension CommentViewController: UITableViewDataSource{
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.photo.comments.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("commentCell") as! CommentCell
        cell.comment = self.photo.comments[indexPath.row]
        cell.delegate = self
        return cell
    }
    

}

//extension CommentViewController: UITableViewDelegate{
// 
//}

extension CommentViewController: UITextViewDelegate{
    func textViewDidEndEditing(textView: UITextView) {
        setPlaceholderForTextView(textView)
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.textColor == UIColor.grayColor(){
            textView.text = ""
            textView.textColor = UIColor.blackColor()
           
        }
    }
    
    func textViewDidChange(textView: UITextView) {
        if textView.text == "" {
            postButton.enabled = false
        }else{
            postButton.enabled = true
        }
    }
    
    func setPlaceholderForTextView(textView: UITextView){
        if textView.text == "" {
            commentTextView.text = "Write a comment"
            commentTextView.textColor = UIColor.grayColor()
        }
    }
}

extension CommentViewController: CommentCellDelegate{
    func commentCell(commentCell: CommentCell, didTapOnIconImageView: Bool) {
        self.performSegueWithIdentifier("segueCommentVCToPeopleVC", sender: commentCell)
    }
}



