//
//  GroupProfileCell.swift
//  FlickrFun
//
//  Created by Loka on 06/05/2016.
//  Copyright © 2016 Loka Studio. All rights reserved.
//

import UIKit

class GroupProfileCell: UICollectionViewCell {
    
    @IBOutlet weak var groupInfoLabel: UILabel!
    @IBOutlet weak var joinLeaveButton: UIButton!
    @IBOutlet weak var photoLabel: UILabel!
    @IBOutlet weak var memberLabel: UILabel!
    @IBOutlet weak var groupImageView: UIImageView!
    var group: Group!{
        didSet{
            setJoinLeaveButtonTitle()
            groupImageView.sd_setImageWithURL(group.coverPhotoURL, placeholderImage: UIImage.init(named: "Placeholder"))
            memberLabel.text = "\(group.numberOfGroupMembers)\n members"
            photoLabel.text = "\(group.numberOfPhotos)\n photos"
        }
    }
    
    func setJoinLeaveButtonTitle(){
        if group.isMember == true{
            joinLeaveButton.setTitle("Leave Group", forState: .Normal)
        }else{
            joinLeaveButton.setTitle("Join Group", forState: .Normal)
        }
    }
    
}
